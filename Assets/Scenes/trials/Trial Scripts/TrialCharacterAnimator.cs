﻿using System.Collections;
using UnityEngine.AI;
using UnityEngine;

public class TrialCharacterAnimator : MonoBehaviour
{
    public AnimationClip replaceableAttackAnim;
    public AnimationClip[] defaultAttackAnimSet;

    public float DelayTime = 20f;

    protected AnimationClip[] currentAttackAnimSet;

    public float agentDelayToResume = 2.5f;

    const float locomationAnimationSmoothTime = .1f;

    NavMeshAgent agent;
    public Animator animator { get; private set; }
    
    public AnimatorOverrideController overrideController;
    

    protected virtual void Start () {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        

        if (overrideController == null)
        {
            overrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        }
        animator.runtimeAnimatorController = overrideController;

        currentAttackAnimSet = defaultAttackAnimSet;

        OnAttack();

        Invoke("Pause", DelayTime);
    }

    void Pause()
    {
        animator.enabled = false;
    }

    protected virtual void Update () {
        //float speedPercent = agent.velocity.magnitude / agent.speed;
        animator.SetBool("inCombat", false);
        //animator.SetFloat("speedPercent", speedPercent, locomationAnimationSmoothTime, Time.deltaTime);

    }

    protected virtual void OnAttack()
    {
        StopAgent();

        int attackIndex = Random.Range(0, currentAttackAnimSet.Length);
        overrideController[replaceableAttackAnim.name] = currentAttackAnimSet[attackIndex];

        //animator.SetTrigger("attack");

        //Debug.Log("Anim: " + overrideController[replaceableAttackAnim.name]);

        ResumeAgent();
    }

    void OnDamage(int maxHealth, int currentHealth)
    {
        StopAgent();
        
        animator.SetTrigger("damage");
        
        ResumeAgent();
    }

    void StopAgent()
    {
        agent.isStopped = true;
        //Debug.Log("agent stopped");
    }

    void ResumeAgent()
    {
        StartCoroutine(DelayedAgentResume());
    }

    IEnumerator DelayedAgentResume()
    {
        yield return new WaitForSeconds(agentDelayToResume);
        
    }
}
