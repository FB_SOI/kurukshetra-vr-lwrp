﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(AudioSource))]
public class DemoSFX : MonoBehaviour
{
    public TextMeshProUGUI text;

    private AudioSource audioSource;

    public void OnHoverEnter()
    {
        // play the ui hover effect
        audioSource.Play();
    }

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        InvokeRepeating("RandomText", 2, 5);
    }
    
    void RandomText()
    {
        text.text = Random.Range(0, 100).ToString();
    }
}
