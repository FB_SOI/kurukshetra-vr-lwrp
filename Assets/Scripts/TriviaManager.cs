﻿using UnityEngine;
using TMPro;

/// <summary>
/// Handle Trivia Related stuff
/// Shows Random Trivia
/// </summary>
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class TriviaManager : MonoBehaviour
{
    public TriviaScriptableObject[] trivias;

    [Header("UI Elements")]
    public GameObject triviaUI;
    public TextMeshProUGUI headerText;
    public TextMeshProUGUI triviaText;

    [Header("Sound Clips")]
    public AudioClip hoverOverSFX;
    public AudioClip orbPopupSFX;

    [SerializeField]
    private float minWaitTime = 20f;

    private Animator animator;
    private AudioSource audioSource;
    private float randomWaitTime { get { return Random.Range(minWaitTime, minWaitTime * 3); } }
    private float orbDisplayTime = 5f;
    
    public void OnHoverEnter()
    {
        audioSource.PlayOneShot(hoverOverSFX);

        animator.SetTrigger("Hover Enter");
        
        CancelInvoke();
    }
    
    public void OnHoverExit()
    {
        animator.SetTrigger("Hover Exit");

        Invoke("PopUpOrb", randomWaitTime);
    }

    public void OnSelected()
    {
        animator.SetTrigger("Slide Down");

        ShowTrivia();
    }

    public void CloseTrivia()
    {
        triviaUI.SetActive(false);

        Invoke("PopUpOrb", randomWaitTime);
    }
    
    void ShowTrivia()
    {
        CancelInvoke();

        triviaUI.SetActive(true);

        var trivia = trivias[Random.Range(0, trivias.Length)];
        headerText.text = trivia.header;
        triviaText.text = trivia.trivia;

    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();

        CloseTrivia();
    }
    
    void PopUpOrb()
    {
        animator.SetTrigger("Slide Up");
        audioSource.PlayOneShot(orbPopupSFX);

        Invoke("HideOrb", orbDisplayTime);
    }

    void HideOrb()
    {
        animator.SetTrigger("Slide Down");
        audioSource.PlayOneShot(orbPopupSFX);

        Invoke("PopUpOrb", randomWaitTime);
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            OnSelected();
        }
    }
#endif
}
