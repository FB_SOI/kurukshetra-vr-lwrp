﻿using UnityEngine;

public class HintManager : MonoBehaviour
{
    public GameObject appIcon;
    public TMPro.TextMeshProUGUI hintText;
    public OVRScreenFade screenFade;

    private GameManagement manager;
    private float waitToFadeTime = 5f;

    private void Awake()
    {
        manager = GameManagement.Instance;

        if (!manager)
        {
            throw new MissingReferenceException(Strings.MISSING_GAMEMANAGEMENT);
        }

        var weapon = manager.weapon;
        if (weapon)
        {
            hintText.gameObject.SetActive(true);
            appIcon.SetActive(false);

            hintText.text = weapon.usage;
        }
        else
        {
            hintText.gameObject.SetActive(false);
            appIcon.SetActive(true);
        }

        if (screenFade)
        {
            StartCoroutine(FadeScreenOut());
        }
    }

    System.Collections.IEnumerator FadeScreenOut()
    {
        yield return new WaitForSeconds(waitToFadeTime);

        StartCoroutine(screenFade.Fade(0, 1));
    }
}
