﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using EZObjectPools;

/// <summary>
/// Contains Information about perticular wave
/// </summary>
[System.Serializable]
public class Wave
{
    [Header("Wave Property")]
    public int TotalEnemies;
    public int ScoreIncrementOnKill = 1;
    public float timeBetweenEnemies = 2f;
    public GameObject Enemy;

    [Header("Enemy Property")]
    public float speed = 3f;
    public int maxHealth = 100;
}

public class EnemySpawnManager : MonoBehaviour
{
    #region Singleton
    public static EnemySpawnManager Instance;
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            throw new System.Exception(string.Format(Strings.SINGLETON_EXISTS, "EnemySpawnManager"));
    }
    #endregion

    [Header("Enemy Waves")]
    public Wave[] waves; // class to hold information per wave

    [Tooltip("Assign any character from family to the character property field")]
    public SpawnPoints[] spawnPoints;

    [Tooltip("Time to start new wave")]
    public float waveWaitTime = 3f;
    public Animator waveUIAnimator;
    public TMPro.TextMeshPro waveUIText;

    [Header("Enemy Impacts Pools")]
    public EZObjectPool enemyImapactPool;
    public EZObjectPool enemyDeathPool;
    public EZObjectPool impactSoundObjectPool;

    public System.Action OnStartNewWave;

    private float timeBetweenEnemies = 2f;
    private int totalEnemiesInCurrentWave;
    private int spawnedEnemiesInCurrentWave;
    private int currentWave;
    private int enemiesSpawned;
    private int enemiesLeftInCurrentWave;

    private CharacterScriptableObject player;
    private CharacterFamily enemyFamily;
    private List<GameObject> pooledEnemies;

    private List<SpawnPoints> pandavasSpawnPoints;
    private List<SpawnPoints> kauravasSpawnPoints;

    private ScoreManager scoreManager;
    private bool playerDefeated = false;

    /// <summary>
    /// On player defeat
    /// </summary>
    public void OnDefeat()
    {
        playerDefeated = true;

        waveUIText.text = Strings.YOU_LOST;
        waveUIAnimator.SetTrigger("ShowWinUI");

        AudioManager.Instance.Play("Wave");

        // disable all active enemies
        for (int i = 0; i < spawnedEnemiesInCurrentWave; i++)
        {
            pooledEnemies[enemiesSpawned - i - 1].SetActive(false);
        }

        Invoke("ShowExitGUI", 3f);
    }

    private void OnWin()
    {
        waveUIText.text = Strings.YOU_WON;
        waveUIAnimator.SetTrigger("ShowWinUI");

        AudioManager.Instance.Play("Wave");

        Invoke("ShowExitGUI", 3f);
    }
    
    private void ShowExitGUI()
    {
        GameManagement.Instance.ShowExitGUI(false);
    }

    private void OnStartGame ()
    {
        if (enemyFamily == CharacterFamily.Pandava)
        {
            waveUIText.text = Strings.KAURAVA_START_GAME;
        }
        else
        {
            waveUIText.text = Strings.PANDAVA_START_GAME;
        }
        waveUIAnimator.SetTrigger("ShowWinUI");

        AudioManager.Instance.Play("Wave");

        Invoke("StartNextWave", waveWaitTime * 2);
    }

    void Start()
    {
        GameManagement gameManager = GameManagement.Instance;
        scoreManager = ScoreManager.Instance;

        //rotate Wave UI if pandava
        if (gameManager.player.family == CharacterFamily.Pandava)
        {
            waveUIText.transform.rotation *= Quaternion.Euler(0, 180, 0);
        }

        if (!gameManager || !scoreManager)
        {
            throw new MissingReferenceException("Cannont find GameManagement");
        }
        player = gameManager.player;
        enemyFamily = (player.family == CharacterFamily.Pandava) ? CharacterFamily.Kaurava : CharacterFamily.Pandava;

        InitializeList();
        PoolEnemies();
        
        currentWave = -1; // avoid off by 1

        // start next wave with delay 
        Invoke("OnStartGame", waveWaitTime);
    }

    private void InitializeList()
    {
        pandavasSpawnPoints = new List<SpawnPoints>();
        kauravasSpawnPoints = new List<SpawnPoints>();

        foreach (SpawnPoints point in spawnPoints)
        {
            if (point.character.family == CharacterFamily.Pandava)
            {
                pandavasSpawnPoints.Add(point);
            }
            else
            {
                kauravasSpawnPoints.Add(point);
            }
        }
    }

    void StartNextWave()
    {
        currentWave++;

        // Invoke callbacks
        OnStartNewWave?.Invoke();

        waveUIText.text = string.Format(Strings.WAVE_NUMBER, currentWave + 1);
        waveUIAnimator.SetTrigger("ShowUI");

        AudioManager.Instance.Play("Wave");
        
        // win
        if (currentWave >= waves.Length)
        {
            OnWin();
            return;
        }

        totalEnemiesInCurrentWave = waves[currentWave].TotalEnemies;
        timeBetweenEnemies = waves[currentWave].timeBetweenEnemies;
        spawnedEnemiesInCurrentWave = 0;
        enemiesLeftInCurrentWave = 0;
        StartCoroutine(SpawnEnemies());
    }
    
    // Coroutine to spawn all of our enemies
    IEnumerator SpawnEnemies()
    {
        while (spawnedEnemiesInCurrentWave < totalEnemiesInCurrentWave)
        {
            spawnedEnemiesInCurrentWave++;
            enemiesLeftInCurrentWave++;

            // Initialize Enemy
            var enemy = pooledEnemies[enemiesSpawned++];
            var wave = waves[currentWave];
            enemy.SetActive(true);
            enemy.GetComponent<EnemyController>().Initialize(wave.speed, wave.maxHealth);
            
            yield return new WaitForSeconds(timeBetweenEnemies);
        }
        yield return null;
    }

    private Transform GetRandomSpawnPoint(SpawnPoints[] points, CharacterFamily family)
    {
        foreach (SpawnPoints point in points)
        {
            if (point.character.family == family)
            {
                return point.spawnPoints[UnityEngine.Random.Range(0, point.spawnPoints.Length)];
            }
        }

        // if not fount, return current transform
        return transform;
    }

    private void PoolEnemies ()
    {
        pooledEnemies = new List<GameObject>();
        int enemyNumber = 0;
        foreach (var wave in waves)
        {
            var enemy = wave.Enemy;
            for (int i = 0; i < wave.TotalEnemies; i++)
            {
                // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
                var tmpPoint = GetRandomSpawnPoint(spawnPoints, enemyFamily);
                var obj = Instantiate(enemy, tmpPoint.position, tmpPoint.rotation, transform) as GameObject;
                var objStats = obj.GetComponent<CharacterStats>();
                if (objStats)
                {
                    objStats.OnDie += EnemyDefeated;
                }
                obj.SetActive(false);
                enemyNumber++;
                pooledEnemies.Add(obj);
            }
        }

    }

    // called by an enemy when they're defeated
    public void EnemyDefeated()
    {
        scoreManager.AddToScore(waves[currentWave].ScoreIncrementOnKill);

        enemiesLeftInCurrentWave--;

        // We start the next wave once we have spawned and defeated them all
        if (!playerDefeated && enemiesLeftInCurrentWave == 0 && spawnedEnemiesInCurrentWave == totalEnemiesInCurrentWave)
        {
            // start next wave with delay
            Invoke("StartNextWave", waveWaitTime);
        }
    }
    
}
