﻿using UnityEngine;

/// <summary>
/// Manages Activating and disabling cannon groups
/// </summary>
public class CannonGroupManager : MonoBehaviour
{
    public CannonManager[] pandavasCannons;
    public CannonManager[] kauravasCannons;

    [Tooltip("Time Duration after cannon deactivates")]
    public float activationDuration = 10f;

    private CannonManager[] cannonsToFire;
    private GameManagement gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManagement.Instance;

        CannonManager[] ownCannons = null;

        // decide which cannons to activate
        if (gameManager.player.family == CharacterFamily.Kaurava)
        {
            cannonsToFire = pandavasCannons;
            ownCannons = kauravasCannons;
        }
        else
        {
            cannonsToFire = kauravasCannons;
            ownCannons = pandavasCannons;
        }

        // disble isTrigger on own cannons, to not recieve onTrigger callback
        // but enable collider to have impacts to happen
        foreach (var cannon in ownCannons)
        {
            cannon.GetComponent<Collider>().isTrigger = false;
        }

        // Subscribe to spawner for new wave start event
        var enemySpawner = EnemySpawnManager.Instance;
        if (enemySpawner)
        {
            enemySpawner.OnStartNewWave += OnStartNewWave;
        }
    }
    
    private void OnStartNewWave()
    {
        foreach (var cannon in cannonsToFire)
        {
            cannon.StartSpawningCannonBalls();

            // deactivate cannons after activation duration 
            cannon.Invoke("StopSpawningCannonBalls", activationDuration);
        }
    }
}