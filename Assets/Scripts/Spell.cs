﻿using UnityEngine;

/// <summary>
/// Handles Spell related settings
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class Spell : MonoBehaviour
{
    // delegates to handle hit events
    public delegate void OnHitEvent(CharacterStats targetStats);
    public OnHitEvent onHitEvent;
    
    //public float impactRadiius = 5f;
    //public LayerMask impactLayerMask;
    
    private void OnCollisionEnter(Collision other)
    {
        //ContactPoint contact = other.contacts[0];

        //Collider[] hitColliders = Physics.OverlapSphere(contact.point, impactRadiius, impactLayerMask);

        //foreach (var col in hitColliders)
        //{
        //    CharacterStats stats = col.transform.GetComponent<CharacterStats>();
        //    if (stats)
        //    {
        //        onHitEvent.Invoke(stats);
        //    }
        //}

        // spell projector listens at onHitEvent. So just call it
        CharacterStats stats = other.transform.GetComponent<CharacterStats>();
        if (stats)
        {
            onHitEvent?.Invoke(stats);
        }
        
        gameObject.SetActive(false);

        //Destroy(this.gameObject);
    }

    private void OnDisable()
    {
        // clear all listeners
        onHitEvent = null;
    }
}
