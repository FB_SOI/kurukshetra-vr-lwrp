﻿using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    #region Singleton

    public static ScoreManager Instance;

    void Awake()
    {
        if (Instance != null)
        {
            Debug.LogWarning(string.Format(Strings.SINGLETON_EXISTS, "ScoreManager"));
            return;
        }

        Instance = this;
    }

    #endregion
    //[HideInInspector]
    public CurvedUI.CurvedUISettings curvedSettings;
    //[HideInInspector]
    public TMPro.TextMeshProUGUI scoreText;
    
    public int score;

    private bool isDisplayScore;

    public void OnHoverEnter()
    {
        curvedSettings.Angle = 90;
        isDisplayScore = true;
    }

    public void OnHoverExit()
    {
        curvedSettings.Angle = 360;
        isDisplayScore = false;
    }

    public void DisplayScore()
    {
        scoreText.text = score.ToString();
    }

    public void HideScore()
    {
        scoreText.text = " ";
    }

    public void AddToScore(int increment)
    {
        score += increment;
    }

    public void SaveScore()
    {
        int highScore = PlayerPrefs.GetInt(PlayerPrefsKeys.PLAYER_HIGH_SCORE, 0);
        highScore = highScore > score ? highScore : score;
        PlayerPrefs.SetInt(PlayerPrefsKeys.PLAYER_HIGH_SCORE, highScore);
    }

    private void Start()
    {
        OnHoverExit();
    }

    private void Update()
    {
        if (isDisplayScore)
        {
            DisplayScore();
        }
        else
        {
            HideScore();
        }
    }

    private void OnDestroy()
    {
        SaveScore();
    }
}
