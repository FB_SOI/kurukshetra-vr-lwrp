﻿using System.Collections;
using TMPro;
using UnityEngine;


/// <summary>
/// Used for Managing Cannons.
/// Update: Now used CannonPoolManager to spawn cannons
/// </summary>
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Animator))]
public class CannonManager : MonoBehaviour
{
    public Transform spawnPoint;
    //public Rigidbody cannonPrefab;
    
    public int numOfCannonBalls = 30;
    public int cannonSpawnMinWaitTime = 5;

    [Range(30, 90)]
    public int minProjectileAngle = 50;
    [Range(30, 90)]
    public int maxProjectileAngle = 62;

    public ParticleSystem cannonFireFX;
    public TextMeshPro countDownText;
    public GameObject arrowPointer;
    public AudioClip cannonFireAudioFX;
    public AudioClip counterAudioFX;
    
    private CannonPoolManager poolManager;
    private Transform playerTransform;
    private Animator cannonFireAnim;
    private int cannonsSpawned = 0;
    private AudioSource audioSource;

    public void StartSpawningCannonBalls()
    {
        StopSpawningCannonBalls();

        cannonsSpawned = 0;
        StartCoroutine(SpawnCannonBall());
    }

    public void StopSpawningCannonBalls()
    {
        CancelInvoke();
        StopAllCoroutines();
    }

    IEnumerator ActivateCannon()
    {
        int timeToStart = 3;
        while (timeToStart > 0)
        {
            audioSource.PlayOneShot(counterAudioFX);
            countDownText.SetText(timeToStart.ToString());
            timeToStart--;
            yield return new WaitForSeconds(1);
        }
        countDownText.enabled = false;

        StartSpawningCannonBalls();
    }

    IEnumerator SpawnCannonBall()
    {
        while (cannonsSpawned++ < numOfCannonBalls)
        {
            if (cannonsSpawned != 1)
                yield return new WaitForSeconds(Random.Range(cannonSpawnMinWaitTime, 2 * cannonSpawnMinWaitTime));

            //var cannon = Instantiate(cannonPrefab, spawnPoint.position, Quaternion.identity, transform);
            GameObject cannon;
            if (poolManager.cannonObjectPool.TryGetNextObject(spawnPoint.position, Quaternion.identity, out cannon))
            {
                // get projectile script and initialize
                Projectile cannonProjectile = cannon.GetComponent<Projectile>();

                cannonProjectile.Initialize(playerTransform, 1f, Random.Range(minProjectileAngle, maxProjectileAngle));
                cannonProjectile.Launch();
                PlayCannonFireFX();
            }
            else
            {
                Debug.LogError(string.Format(Strings.UNABLE_TO_SPAWN, "Cannon"));
            }

            //// get projectile script and initialize
            //Projectile cannonProjectile = cannon.GetComponent<Projectile>();
            ////cannonProjectile.Initialize(playerTransform, 1f, 60);
            //cannonProjectile.Initialize(playerTransform, 1f, Random.Range(50, 62));
            //cannonProjectile.Launch();
            //PlayCannonFireFX();
            //// missileProjectile.Invoke("Launch", Random.value * 10f);
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Cannon: " + other.transform.tag);
        arrowPointer.SetActive(false);
        countDownText.enabled = true;
        StartCoroutine(ActivateCannon());

        // disable collider, so we can't get calls to this function again
        GetComponent<Collider>().enabled = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        cannonFireAnim = GetComponent<Animator>();
        playerTransform = PlayerSelection.Instance.currentPlayer.transform;
        poolManager = CannonPoolManager.Instance;

        if (!cannonFireFX || !playerTransform)
        {
            throw new MissingComponentException();
        }
        //InvokeRepeating("SpawnCannon", 10, 5);

        countDownText.enabled = false;
    }
    
    void PlayCannonFireFX()
    {
        audioSource.PlayOneShot(cannonFireAudioFX);
        cannonFireAnim.SetTrigger("Fire");
        cannonFireFX.Play();
    }
}
