﻿using System.Collections;
using UnityEngine;

/// <summary>
/// used to control behaviour of projectiles like cannons.
/// </summary>
public class Projectile : MonoBehaviour
{
    // launch variables
    [SerializeField] private Transform TargetObjectTF;
    [Range(1.0f, 15.0f)] public float TargetRadius;
    [Range(20.0f, 75.0f)] public float LaunchAngle;
    public bool DestroyObjectOnTouchingGround = true;
    public float disableAfterTime = 5f;
    private int damegeValue = 10;

    private bool isTouchingGround;
    private bool isDestroyed;
    
    private Rigidbody rigid;
    private Vector3 initialPosition;
    private Quaternion initialRotation;
    
    public void Initialize(Transform TargetObjectTF, float TargetRadius, float LaunchAngle)
    {
        this.TargetObjectTF = TargetObjectTF;
        this.TargetRadius = TargetRadius;
        this.LaunchAngle = LaunchAngle;

        GetComponent<Collider>().enabled = true;
        rigid = GetComponent<Rigidbody>();
        isTouchingGround = false;
        isDestroyed = false;
        initialPosition = transform.position;
        initialRotation = transform.rotation;
    }

    /// <summary>
    /// Launches the object towards the TargetObject with a given LaunchAngle
    /// </summary>
    public void Launch()
    {
        // think of it as top-down view of vectors: 
        //   we don't care about the y-component(height) of the initial and target position.
        Vector3 projectileXZPos = new Vector3(transform.position.x, 0.0f, transform.position.z);
        Vector3 targetXZPos = new Vector3(TargetObjectTF.position.x, 0.0f, TargetObjectTF.position.z);

        // rotate the object to face the target
        transform.LookAt(targetXZPos);

        // shorthands for the formula
        float R = Vector3.Distance(projectileXZPos, targetXZPos);
        float G = Physics.gravity.y;
        float tanAlpha = Mathf.Tan(LaunchAngle * Mathf.Deg2Rad);
        float H = TargetObjectTF.position.y - transform.position.y;

        // calculate the local space components of the velocity 
        // required to land the projectile on the target object 
        float Vz = Mathf.Sqrt(G * R * R / (2.0f * (H - R * tanAlpha)));
        float Vy = tanAlpha * Vz;

        // create the velocity vector in local space and get it in global space
        Vector3 localVelocity = new Vector3(0f, Vy, Vz);
        Vector3 globalVelocity = transform.TransformDirection(localVelocity);

        // launch the object by setting its initial velocity and flipping its state
        rigid.velocity = globalVelocity;

        StartCoroutine(DelayedDisble());
    }

    void OnCollisionEnter(Collision other)
    {
        OnImpact();
    }

    private void OnTriggerEnter(Collider other)
    {
        //bTouchingGround = true;
        //if (DestroyObjectOnTouchingGround)
        //{
        //    Destroy(gameObject); // destroy immediately as it will be shield or arrow
        //}
        //if (other.CompareTag("Shield"))
        //{
        //    IHitable obj = GetComponent(typeof(IHitable)) as IHitable;
        //    if (obj != null)
        //    {
        //        obj.Hit();
        //    }
        //}
        // if hitting any player, damage them
        if (other.transform.CompareTag("Player"))
        {
            //Debug.Log("Hitting player from cannon");
            CharacterStats stats = PlayerSelection.Instance.transform.GetComponent<CharacterStats>();
            if (stats)
            {
                stats.TakeDamage(damegeValue);
            }
            OnImpact();
        }
        // Do the impact logic only if trigger is shield or mace
        else if (other.CompareTag(Strings.SHIELD_TAG) || other.CompareTag(Strings.MACE_TAG))
        {
            OnImpact();
        }
    }

    void OnImpact()
    {
        // disable collider on object to hadle damaging object by bouncing on surfaces.
        GetComponent<Collider>().enabled = false;

        isTouchingGround = true;

        if (!isDestroyed)
        {
            IHitable obj = GetComponent(typeof(IHitable)) as IHitable;
            if (obj != null)
            {
                obj.Hit();
            }
            isDestroyed = true;
        }
    }

    private IEnumerator DelayedDisble()
    {
        yield return new WaitForSeconds(disableAfterTime);
        gameObject.SetActive(false);
    }

    void OnCollisionExit()
    {
        isTouchingGround = false;
    }
    
}
