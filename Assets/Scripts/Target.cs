﻿using UnityEngine;

/// <summary>
/// Any target that can be hitted
/// </summary>
public class Target : MonoBehaviour, IHitable
{
    [SerializeField]
    ParticleSystem shootEffect;

    public void Hit()
    {
        shootEffect.Play(true);
    }

    void Start()
    {
        shootEffect = GetComponentInChildren<ParticleSystem>();
    }
    
}
