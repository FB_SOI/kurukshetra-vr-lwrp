﻿using UnityEngine;

/// <summary>
/// Used for Selection of weapons for training arena
/// </summary>
public class WeaponSelector : MonoBehaviour
{
    public WeaponScriptableObject weapon1;
    public WeaponScriptableObject weapon2;

    private GameManagement gameManager;

    public void OnSelectedFirstWeapon()
    {
        gameManager.SavePlayer(weapon1);
        LoadScene();
    }

    public void OnSelectedSecondWeapon()
    {
        gameManager.SavePlayer(weapon2);
        LoadScene();
    }

    private void LoadScene()
    {
        gameManager.LoadScene(Scenes.Arena);
    }

    private void Awake()
    {
        gameManager = GameManagement.Instance;
        if (gameManager == null)
        {
            throw new MissingReferenceException("Cannont find GameManagement");
        }
    }
    
}
