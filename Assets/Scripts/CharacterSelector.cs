﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterSelector : MonoBehaviour
{
    [Header("Character")]
    public GameObject characterSelectionUI;
    public List<CharacterScriptableObject> characterGameObjects;
    //public Transform pandavasSpawnPoint;
    //public Transform kauravasSpawnPoint;

    [Header("UI Pandava")]
    public Image pandavasCharacterImage;
    public TextMeshProUGUI pandavasCharacterName;
    public TextMeshProUGUI pandavasCharacterDesc;
    public Image pandavaSelectionBackground;

    [Header("UI Kauravas")]
    public Image kauravasCharacterImage;
    public TextMeshProUGUI kauravasCharacterName;
    public TextMeshProUGUI kauravasCharacterDesc;
    public Image kauravaSelectionBackground;

    private CharacterFamily selectedCharacterFamily { get { return selectedPlayer.family; } }
    private CharacterFamily selectedOpponentFamily { get { return selectedCharacterFamily == CharacterFamily.Pandava ? CharacterFamily.Kaurava : CharacterFamily.Pandava; } }
    
    private List<CharacterScriptableObject> kauravas;
    private List<CharacterScriptableObject> pandavas;

    private int kauravasSelectedIndex = 0;
    private int pandavasSelectedIndex = 0;

    private CharacterScriptableObject selectedPlayer;
    private CharacterScriptableObject opponentPlayer;

    private float disabledAlpha = 0.33f;
    private float enabledAlpha = 1f;

    //private GameObject kauravasSelectedObject;
    //private GameObject pandavasSelectedObject;

    private GameManagement gameManager;

    public void ToogleUI(bool isActive)
    {
        characterSelectionUI.SetActive(isActive);
    }

    // using wrapper method to convert into enum
    public void DisplayNextCharacter(int family)
    {
        Debug.Log("DisplayNext " + (CharacterFamily)family);
        DisplayNextCharacter((CharacterFamily)family);
    }

    private void DisplayNextCharacter(CharacterFamily family)
    {
        Debug.Log("changing: " + family);
        
        var characters = GetListByFamily(family);

        if (characters == null || characters.Count == 0)
        {
            return;
        }
        
        if (family == CharacterFamily.Kaurava)
        {
            kauravasSelectedIndex++;
            kauravasSelectedIndex %= characters.Count;
            
            selectedPlayer = characters[kauravasSelectedIndex];
        }
        else if (family == CharacterFamily.Pandava)
        {
            pandavasSelectedIndex++;
            pandavasSelectedIndex %= characters.Count;
            selectedPlayer = characters[pandavasSelectedIndex];
        }

        ChangeSelectedPlayer(selectedPlayer);

        DisplayCharacter(selectedPlayer);
        DisplayOpponent();
    }
    
    private void ChangeSelectedPlayer(CharacterScriptableObject character)
    {
        selectedPlayer = character;

        // setting UI color
        var tmpColor = kauravaSelectionBackground.color;

        if (character.family == CharacterFamily.Kaurava)
        {
            tmpColor.a = disabledAlpha;
            pandavaSelectionBackground.color = tmpColor;

            tmpColor.a = enabledAlpha;
            kauravaSelectionBackground.color = tmpColor;
        }
        else
        {
            tmpColor.a = disabledAlpha;
            kauravaSelectionBackground.color = tmpColor;

            tmpColor.a = enabledAlpha;
            pandavaSelectionBackground.color = tmpColor;
        }
        //SaveFamilyPrefs();
    }
    

    private void Awake()
    {
        //gameManager = FindObjectOfType<GameManagement>();
        gameManager = GameManagement.Instance;
        if (gameManager == null)
        {
            throw new MissingReferenceException("Cannont find GameManagement");
        }

        Initialize();
    }

    private void Initialize()
    {
        InitializeLists();
        
        // set default player
        ChangeSelectedPlayer(pandavas[0]);
        
        DisplayCharacter(selectedPlayer);
        DisplayOpponent();
    }
    private void InitializeLists()
    {
        kauravas = new List<CharacterScriptableObject>();
        pandavas = new List<CharacterScriptableObject>();

        foreach (var character in characterGameObjects)
        {
            if (character.family == CharacterFamily.Kaurava)
            {
                kauravas.Add(character);
            }
            else if (character.family == CharacterFamily.Pandava)
            {
                pandavas.Add(character);
            }
        }
    }

    private void DisplayCharacter(CharacterScriptableObject character)
    {
        DisplayUI(character);
    }

    private void DisplayUI(CharacterScriptableObject character)
    {
        if (character.family == CharacterFamily.Pandava)
        {
            pandavasCharacterName.text = character.characterName;
            pandavasCharacterImage.sprite = character.characterSprite;
            pandavasCharacterDesc.text = character.characterDescription;
        }
        else
        {
            kauravasCharacterName.text = character.characterName;
            kauravasCharacterImage.sprite = character.characterSprite;
            kauravasCharacterDesc.text = character.characterDescription;
        }
    }
    private void DisplayOpponent()
    {
        var characters = GetListByFamily(selectedOpponentFamily);
        int index = 0;
        if (characters == null || characters.Count <= index)
        {
            return;
        }

        Weapon characterWeapon;
        if (selectedCharacterFamily == CharacterFamily.Kaurava)
        {
            characterWeapon = kauravas[kauravasSelectedIndex].weapon.weapon;
        }
        else/* if (selectedCharacterFamily == CharacterFamily.Pandava)*/
        {
            characterWeapon = pandavas[pandavasSelectedIndex].weapon.weapon;
        }


        foreach (var opponent in characters)
        {
            if (opponent.weapon.weapon == characterWeapon)
            {
                if (selectedOpponentFamily == CharacterFamily.Pandava)
                {
                    pandavasSelectedIndex = index;
                    break;
                }
                else /*if (selectedOpponentFamily == CharacterFamily.Pandava)*/
                {
                    kauravasSelectedIndex = index;
                    break;
                }
            }
            index++;
        }

        if (index < characters.Count)
        {
            opponentPlayer = characters[index];

            DisplayCharacter(opponentPlayer);
        }

    }
    /// <summary>
    /// Saves Character Selection related info to GameManagement
    /// And Call Next Scene
    /// </summary>
    public void SaveAndLoad()
    {
        SaveCharacterSelection();
        gameManager.LoadScene(Scenes.Game);
    }
    
    private void SaveCharacterSelection()
    {
        gameManager.SavePlayer(selectedPlayer);
        gameManager.SaveOpponent(opponentPlayer);
    }

    private void SaveFamilyPrefs()
    {
        PlayerPrefs.SetInt(PlayerPrefsKeys.PLAYER_WEAPON_PREFS, (int) selectedPlayer.weapon.weapon);
        PlayerPrefs.SetInt(PlayerPrefsKeys.OPPONENT_WEAPON_PREFS, (int) opponentPlayer.weapon.weapon);
    }
    
    List<CharacterScriptableObject> GetListByFamily(CharacterFamily family)
    {
        switch (family)
        {
            case CharacterFamily.Pandava:
                return pandavas;
            case CharacterFamily.Kaurava:
                return kauravas;
        }
        return null;
    }
}
