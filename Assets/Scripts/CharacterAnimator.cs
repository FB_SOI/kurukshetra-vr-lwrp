﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterAnimator : MonoBehaviour {

    public AnimationClip replaceableAttackAnim;
    public AnimationClip[] defaultAttackAnimSet;
    protected AnimationClip[] currentAttackAnimSet;

    public float agentDelayToResume = 2.5f;

    const float locomationAnimationSmoothTime = .1f;

    NavMeshAgent agent;
    public Animator animator { get; private set; }
    protected CharacterCombat combat;
    public AnimatorOverrideController overrideController;
    public bool isDead = false;

    protected virtual void Start () {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        combat = GetComponent<CharacterCombat>();

        if (overrideController == null)
        {
            overrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        }
        animator.runtimeAnimatorController = overrideController;

        currentAttackAnimSet = defaultAttackAnimSet;
        combat.OnAttack += OnAttack;

        // set damage animation event
        GetComponent<CharacterStats>().OnHealthChanged += OnDamage;
	}
	
	protected virtual void Update () {
        float speedPercent = agent.velocity.magnitude / agent.speed;
        animator.SetFloat("speedPercent", speedPercent, locomationAnimationSmoothTime, Time.deltaTime);

        animator.SetBool("inCombat", combat.InCombat);
	}

    protected virtual void OnAttack()
    {
        StopAgent();

        int attackIndex = Random.Range(0, currentAttackAnimSet.Length);
        overrideController[replaceableAttackAnim.name] = currentAttackAnimSet[attackIndex];

        animator.SetTrigger("attack");

        //Debug.Log("Anim: " + overrideController[replaceableAttackAnim.name]);

        ResumeAgent();
    }

    void OnDamage(int maxHealth, int currentHealth)
    {
        StopAgent();

        //Debug.Log("damaged " + transform.name);
        if (currentHealth <= 0)
        {
            if (!isDead)
            {
                animator.SetTrigger("death");
                isDead = true;
            }
        }
        else
        {
            animator.SetTrigger("damage");
        }

        ResumeAgent();
    }

    void StopAgent()
    {
        agent.isStopped = true;
        //Debug.Log("agent stopped");
    }

    void ResumeAgent()
    {
        StartCoroutine(DelayedAgentResume());
    }

    IEnumerator DelayedAgentResume()
    {
        yield return new WaitForSeconds(agentDelayToResume);
        //Debug.Log("agent resumes");
        if (combat.myStats.currentHealth > 0)
        {
            agent.isStopped = false;
        }
        
    }
}
