using UnityEngine;

/// <summary>
/// Keeps track of enemy stats, loosing health and dying.
/// </summary>
public class EnemyStats : CharacterStats {
    private float disableDelay = 1f;

	public override void Die()
	{
		base.Die();

        OnDie?.Invoke();

        // Add ragdoll effect / death animation
        EnemySpawnManager.Instance.enemyDeathPool.TryGetNextObject(transform.position, Quaternion.Euler(-90, 0, 0));

        PlayOnDieFX();

        StartCoroutine(DelayedDisable());
        //Destroy(gameObject, 1);
        //Destroy(gameObject, 4);
    }

    System.Collections.IEnumerator DelayedDisable()
    {
        yield return new WaitForSeconds(disableDelay);

        gameObject.SetActive(false);
    }

    void PlayOnDieFX()
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        if (audioSource)
        {
            audioSource.Play();
        }
    }
}
