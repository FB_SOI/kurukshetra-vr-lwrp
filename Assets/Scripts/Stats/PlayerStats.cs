using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Handles the players stats and adds/removes modifiers when equipping items.
/// </summary>
public class PlayerStats : CharacterStats {

	// Use this for initialization
	void Start () {
        var manager = EquipmentManager.instance;
        if (manager)
        {
            manager.onEquipmentChanged += OnEquipmentChanged;
        }
	}
	
	// Called when an item gets equipped/unequipped
	void OnEquipmentChanged (Equipment newItem, Equipment oldItem)
	{
		// Add new modifiers
		if (newItem != null)
		{
			armor.AddModifier(newItem.armorModifier);
			damage.AddModifier(newItem.damageModifier);
		}

		// Remove old modifiers
		if (oldItem != null)
		{
			armor.RemoveModifier(oldItem.armorModifier);
			damage.RemoveModifier(oldItem.damageModifier);
		}
		
	}

	public override void Die()
	{
		base.Die();
        var manager = PlayerManager.instance;
        if (manager)
            manager.KillPlayer();
	}
}
