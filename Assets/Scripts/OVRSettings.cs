﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OVRSettings : MonoBehaviour
{
    public bool isEnableFFR = true;
    public bool isEnable72HZ = true;

    // Start is called before the first frame update
    void Start()
    {
        // setting some ovr settings
        if (isEnableFFR)
            OVRManager.tiledMultiResLevel = OVRManager.TiledMultiResLevel.LMSHigh;
        if (isEnable72HZ)
            OVRManager.display.displayFrequency = 72.0f;
    }
    
}
