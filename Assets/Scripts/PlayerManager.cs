using UnityEngine;

/// <summary>
/// Keeps track of the player
/// </summary>
public class PlayerManager : MonoBehaviour {

	#region Singleton

	public static PlayerManager instance;

	void Awake ()
	{
        if (instance != null)
        {
            Debug.LogWarning("Multiple PlayerManager instaces found...");
            return;
        }
		instance = this;
	}

	#endregion

	public GameObject player;
    public Canvas uiCanvas;

    public void KillPlayer ()
	{
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        EnemySpawnManager.Instance.OnDefeat();
	}

}
