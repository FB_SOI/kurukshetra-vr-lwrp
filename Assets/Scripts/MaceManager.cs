﻿using UnityEngine;
using EZObjectPools;

public class MaceManager : MonoBehaviour
{   
    public OVRTrackedRemote trackedObj;

    private GameObject currentMace;
    
    public GameObject macePrefab;

    public EZObjectPool objectPooler;

    #region Singleton
    public static MaceManager Instance;
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            throw new System.Exception(string.Format(Strings.SINGLETON_EXISTS, "MaceManager"));
    }

    void OnDestroy()
    {
        if (Instance == this)
            Instance = null;
    }
    #endregion


    private void Start()
    {
        AttachMace();
    }

    private void AttachMace()
    {
        Debug.Log("Attach Mace");
        if (currentMace == null)
        {
            //currentMace = Instantiate(macePrefab);
            currentMace = macePrefab;
            macePrefab.SetActive(true);

            currentMace.transform.parent = trackedObj.transform;
            currentMace.transform.localPosition = new Vector3(0f, 0f, .8f);
            currentMace.transform.localRotation = Quaternion.identity;
        }
    }
}
