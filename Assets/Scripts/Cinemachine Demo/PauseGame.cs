﻿
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    public float DelayTime = 1f;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("Pause", DelayTime);
    }

    void Pause()
    {
        Time.timeScale = 0;
    }
}
