﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]
public class RunDelayed : MonoBehaviour
{
    public Transform runTowards;
    public float delay = 5f;

    NavMeshAgent agent;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();

       
        StartCoroutine(DelayedRun());
    }

    private void Update()
    {
        agent.SetDestination(runTowards.position);
    }

    IEnumerator DelayedRun()
    {
        yield return new WaitForSeconds(delay);

        Debug.Log("Start Running");
        
        
    }
}
