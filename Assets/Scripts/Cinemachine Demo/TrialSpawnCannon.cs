﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrialSpawnCannon : MonoBehaviour
{
    public Transform spawnPoint;

    [Range(30, 90)]
    public int minProjectileAngle = 50;
    [Range(30, 90)]
    public int maxProjectileAngle = 62;

    public Transform playerTransform;
    public ParticleSystem cannonFireFX;

    private CannonPoolManager poolManager;
    
    private Animator cannonFireAnim;
    // Start is called before the first frame update
    void Start()
    {
        cannonFireAnim = GetComponent<Animator>();
        poolManager = CannonPoolManager.Instance;

        StartCoroutine(SpawnCannonBall());
        Time.timeScale = 0.3f;
    }

    IEnumerator SpawnCannonBall()
    {
        GameObject cannon;
        if (poolManager.cannonObjectPool.TryGetNextObject(spawnPoint.position, Quaternion.identity, out cannon))
        {
            // get projectile script and initialize
            Projectile cannonProjectile = cannon.GetComponent<Projectile>();

            cannonProjectile.Initialize(playerTransform, 1f, Random.Range(minProjectileAngle, maxProjectileAngle));
            cannonProjectile.Launch();
            PlayCannonFireFX();
        }
        else
        {
            Debug.LogError(string.Format(Strings.UNABLE_TO_SPAWN, "Cannon"));
        }
        yield return null;
    }
    void PlayCannonFireFX()
    {
        cannonFireAnim.SetTrigger("Fire");
        cannonFireFX.Play();
    }
}
