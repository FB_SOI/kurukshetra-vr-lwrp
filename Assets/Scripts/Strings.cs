﻿public static class Strings
{
    public static string SPAWN_ERROR_MESSAGE = "Pool with tag {0} doesn't exists !";
    public static string SINGLETON_EXISTS = "Singleton class {0} already defined !";
    public static string COLLISION = "Collision: {0}";
    public static string UNABLE_TO_SPAWN = "Unable to spawn: {0}";
    public static string WAVE_NUMBER = "Wave: {0}";
    public static string MISSING_GAMEMANAGEMENT = "GameManager Missing";
    public static string DEFAULT_HINT_TEXT = "Team Aries Presents...";
    public static string SCORE_TEXT = "High Score : {0}";

    public const string YOU_LOST = "You Lost. Press Back... !";
    public const string YOU_WON = "You Won. Press Back... !";
    public const string SHIELD_TAG = "Shield";
    public const string MACE_TAG = "Mace";
    public const string BOW_TAG = "Bow";

    public const string PANDAVA_START_GAME = "Kill to conquer the fort...!";
    public const string KAURAVA_START_GAME = "Kill to defend the fort...!";
}
