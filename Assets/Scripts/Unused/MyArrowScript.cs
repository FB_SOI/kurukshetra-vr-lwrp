﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MyArrowScript : MonoBehaviour {

    // Use this for initialization

     public int theSpeed = 20;

    public float ArrowLife = 1.0f;

    private bool arrowforward = true;

    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update ()
    {
         if (arrowforward == true)
        {
          this.transform.Translate(Vector3.forward * theSpeed * Time.deltaTime);
         }
        

    ArrowLife -= Time.deltaTime;
        if (ArrowLife <= 0)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag.Contains ("Enemy"))
        {
            Debug.Log("Arrow gets stuck");
            arrowforward = false;
        }

       /*else if(col.gameObject.tag.Contains ("pot"))
        {
           // Debug.Log("pot is hit");
            Destroy(gameObject);
        }*/
    }
}
