﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingScreenManager : MonoBehaviour
{
    public GameObject loadingScreenUI;
    public Slider slider;
    public TextMeshProUGUI loadingText;

    private string loadingTextFormat = "Loading... {0} %";

    public void LoadScene(int buildIndex)
    {
        loadingScreenUI.SetActive(true);
        StartCoroutine(LoadAsyncronously(buildIndex));
    }

    IEnumerator LoadAsyncronously(int buildIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(buildIndex, LoadSceneMode.Single);
        loadingScreenUI.SetActive(true);

        while (!operation.isDone)
        {
            // unity loads assets in 0 to .9 and activates in .9 to 1
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;
            loadingText.text = string.Format(loadingTextFormat, (int) (progress * 100));
            yield return null;
        }
        loadingScreenUI.SetActive(false);
    }

    private void Awake()
    {
        loadingScreenUI.SetActive(false);
    }
}
