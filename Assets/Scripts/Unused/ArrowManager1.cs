﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ArrowManager1 : MonoBehaviour
{
    public Text debugText;
    public static ArrowManager1 Instance;

    public OVRTrackedRemote trackedObj;

    private GameObject currentArrow;

    public GameObject stringAttachPoint;
    public GameObject arrowStartPoint;
    public GameObject stringStartPoint;

    public GameObject arrowPrefab;

    // shield
    public GameObject shield;

    private bool isAttached = false;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        shield.SetActive(false);
    }

    void OnDestroy()
    {
        if (Instance == this)
            Instance = null;
    }

    void Update()
    {
        AttachArrow();
        if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger))
        {
            Fire();
        }
        if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger))
        {
            PullString();
        }

        // making shield work or not
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            shield.SetActive(true);
        }
        if (OVRInput.GetUp(OVRInput.Button.One))
        {
            shield.SetActive(false);
        }
    }

    float dist = 0;
    private void PullString()
    {
        if (isAttached)
        {
            //float dist = (stringStartPoint.transform.position - trackedObj.transform.position).magnitude;
            dist += 0.01f;
            stringAttachPoint.transform.localPosition = stringStartPoint.transform.localPosition + new Vector3(5f * dist, 0f, 0f);
            DrawPath();

            //var device = SteamVR_Controller.Input((int)trackedObj);
            //if (device.GetTouchUp (SteamVR_Controller.ButtonMask.Trigger)) {

        }
    }

    void DrawPath()
    {
        //LaunchData launchData = CalculateLaunchData ();
        Vector3 previousDrawPoint = currentArrow.transform.position;

        // calculationg velocity
        float dist = (stringStartPoint.transform.position - trackedObj.transform.position).magnitude;
        Vector3 initialVelocity = currentArrow.transform.forward * 25f * dist;

        int resolution = 30;
        for (int i = 1; i <= resolution; i++)
        {
            //float simulationTime = i / (float)resolution * launchData.timeToTarget;
            float simulationTime = i / (float)resolution * 1f;
            //Vector3 displacement = launchData.initialVelocity * simulationTime + Vector3.up *gravity * simulationTime * simulationTime / 2f;
            Vector3 displacement = initialVelocity * simulationTime + Vector3.up * -18f * simulationTime * simulationTime / 2f;
            Vector3 drawPoint = currentArrow.transform.position + displacement;
            Debug.DrawLine(previousDrawPoint, drawPoint, Color.red);
            previousDrawPoint = drawPoint;
        }
    }

    private void Fire()
    {
        //float dist = (stringStartPoint.transform.position - trackedObj.transform.position).magnitude;

        currentArrow.transform.parent = null;
        currentArrow.GetComponent<Arrow>().Fired();

        Rigidbody r = currentArrow.GetComponent<Rigidbody>();
        r.velocity = currentArrow.transform.forward * 25f * dist;
        r.useGravity = true;

        currentArrow.GetComponent<Collider>().isTrigger = false;

        stringAttachPoint.transform.position = stringStartPoint.transform.position;

        currentArrow = null;
        isAttached = false;

        // when fired, end slow motion
        Time.timeScale = 1f;

        dist = 0;
    }

    private void AttachArrow()
    {
        if (currentArrow == null)
        {
            currentArrow = Instantiate(arrowPrefab);
            currentArrow.transform.parent = trackedObj.transform;
            currentArrow.transform.localPosition = new Vector3(0f, 0f, .342f);
            currentArrow.transform.localRotation = Quaternion.identity;
        }
    }

    public void AttachBowToArrow()
    {
        currentArrow.transform.parent = stringAttachPoint.transform;
        currentArrow.transform.position = arrowStartPoint.transform.position;
        currentArrow.transform.rotation = arrowStartPoint.transform.rotation;

        isAttached = true;

        // when attached to bow, go into slow motion
        Time.timeScale = 0.5f;
    }

    internal void DebugText(string tag)
    {
        debugText.text = tag;
    }
}