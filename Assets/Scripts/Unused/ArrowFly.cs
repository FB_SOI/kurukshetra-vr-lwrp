﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowFly : MonoBehaviour {

    public float flyTime;
    public Collider childCollider;
    Rigidbody rb;


    private bool flying = true;
    private float stoptime;
    private Transform anchor;

	// Use this for initialization
	void Start () {

        this.stoptime = Time.time + this.flyTime;
        //rb = GetComponent<Rigidbody>();
	}

	
	// Update is called once per frame
	void Update ()
    {
	    if(this.stoptime <= Time.time && this.flying)
        {
            GameObject.Destroy(this.childCollider);
        }

        if(this.flying)
        {
            this.rotate();
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        if(this.flying)
        {
            this.flying = false;
            this.transform.position = collision.contacts[0].point;
            this.childCollider.isTrigger = true;

           Destroy(this.childCollider);
           collision.gameObject.SendMessage("arrowhit", SendMessageOptions.DontRequireReceiver);
        }
    }

    void rotate()
    {
        transform.LookAt(transform.position + rb.velocity);
    }
}
