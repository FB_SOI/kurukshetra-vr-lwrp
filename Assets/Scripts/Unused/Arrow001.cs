﻿
using System;
using UnityEngine;
using UnityEngine.UI;

public class Arrow001 : MonoBehaviour
{
    private bool isAttached = false;

    private bool isFired = false;

    //void OnTriggerStay() {
    //	AttachArrow ();
    //}

    //void OnTriggerEnter(Collider other) {
    //       // ArrowManager.Instance.DebugText(other.tag);
    //       if (other.CompareTag("Bow"))
    //       {
    //           AttachArrow();
    //           return;
    //       }
    //   }


    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("Destructables"))
        {
            IHitable hitable = other.transform.GetComponent(typeof(IHitable)) as IHitable;
            hitable.Hit();
            return;
        }
    }

    //private void DestroyEnemy(GameObject other, float time)
    //{
    //    Destroy(other, time);
    //}

    void Update()
    {
        if (isFired && transform.GetComponent<Rigidbody>().velocity.magnitude > 5f)
        {
            transform.LookAt(transform.position + transform.GetComponent<Rigidbody>().velocity);
        }
        if (!isAttached)
        {
            AttachArrow();
        }
    }

    public void Fired()
    {
        isFired = true;
    }

    private void AttachArrow()
    {
        //var device = SteamVR_Controller.Input((int)ArrowManager.Instance.trackedObj.index);
        //if (!isAttached && device.GetTouch (SteamVR_Controller.ButtonMask.Trigger)) {
        if (!isAttached && OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {
            ArrowManager.Instance.AttachBowToArrow();
            isAttached = true;
        }
    }


}
