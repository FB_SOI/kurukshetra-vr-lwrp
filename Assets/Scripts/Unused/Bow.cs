﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Bow : MonoBehaviour
{

    public int theRange = 1000;
    public LineRenderer theLaser = null;
    public GameObject theArrow = null;
    public GameObject parent;
    private Animator anim;
    
    public float timeBetweenShots = 0.3f;  // Allow 3 shots per second
    private float timestamp;

    private Vector3 thePos = Vector3.zero;
    private Vector3 theDirection = Vector3.zero;
    private Vector3 theEnd = Vector3.zero;

    private RaycastHit theHit;
    private bool isOut = false;

    private void Start()
    {
        theLaser = this.gameObject.GetComponent<LineRenderer>();
        anim = parent.GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        thePos = this.transform.position;
        theDirection = this.transform.TransformDirection(Vector3.forward);

        theLaser.SetPosition(0, thePos);

        if (Physics.Raycast(thePos, theDirection,out theHit, theRange))
        {

            theLaser.SetPosition(1, theHit.point);
        
        }

        theEnd = thePos + theDirection * theRange;
        theLaser.SetPosition(1, theEnd);
    }

    private void Update() 
    {
        if(Time.time >= timestamp && Input.GetKeyDown(KeyCode.Mouse0))
        {

            isOut = true;
            timestamp = Time.time + timeBetweenShots;
            anim.SetTrigger("arrow_reload0");


        }


        if (isOut)
        {
            //Rigidbody clone;
            GameObject cloneArrow = (GameObject) Instantiate(theArrow, thePos, this.transform.rotation);
            cloneArrow.transform.position = thePos;
            cloneArrow.transform.rotation = this.transform.rotation;
            isOut = false;
        }
    }
}
