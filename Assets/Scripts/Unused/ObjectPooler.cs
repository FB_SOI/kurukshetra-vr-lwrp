﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Pool
{
    public string tag;
    public GameObject prefab;
    public int size;
}

public class ObjectPooler : MonoBehaviour
{
    #region Singleton

    public ObjectPooler instance;

    private void Awake()
    {
        if (instance)
        {
            Debug.LogError(string.Format(Strings.SPAWN_ERROR_MESSAGE, "ObjectPooler"));
            return;
        }
        instance = this;
    }

    #endregion
    public Dictionary<string, Queue<GameObject>> poolDictionary;
    public List<Pool> pools;

    // Start is called before the first frame update
    void Start()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject go = Instantiate(pool.prefab);
                go.SetActive(false);
                objectPool.Enqueue(go);
            }

            poolDictionary.Add(pool.tag, objectPool);
        }

    }

    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        if (!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning(string.Format(Strings.SPAWN_ERROR_MESSAGE, tag));
            return null;
        }

        GameObject objectToSpawn = poolDictionary[tag].Dequeue();

        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;

        poolDictionary[tag].Enqueue(objectToSpawn);

        return objectToSpawn;
    }
}
