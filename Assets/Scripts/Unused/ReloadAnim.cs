﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadAnim : MonoBehaviour {

    private int m_LastIndex;

    public void PlayReloadAnim()
    {
        if (!GetComponent<Animation>().isPlaying)
        {
            if (m_LastIndex == 0)
            {
                GetComponent<Animation>().Play("Arrow_reload");
                m_LastIndex = 1;
            }
        }
    }
}
