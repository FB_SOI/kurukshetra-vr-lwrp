﻿using UnityEngine;


public class Bow1 : MonoBehaviour
{
    public GameObject target;

    private void LateUpdate()
    {
        transform.position = target.transform.position
            + target.transform.forward * 10f;

        transform.rotation = new Quaternion(0.0f, target.transform.rotation.y,
            0.0f, target.transform.rotation.w);
    }
}
