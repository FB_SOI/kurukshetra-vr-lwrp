﻿using UnityEngine.UI;
using UnityEngine;

public class BowPosition : MonoBehaviour
{
    public Transform obj;
    public Transform obj2;
    Text debugText;
    // Start is called before the first frame update
    void Start()
    {
        debugText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        debugText.text = obj.position + " " + obj.rotation + "\n" + obj2.position + " " + obj2.rotation;
    }
}
