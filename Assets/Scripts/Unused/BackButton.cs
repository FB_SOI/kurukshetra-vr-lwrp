﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BackButton : MonoBehaviour
{
    public GameObject LoadingScreen;
    public Slider slider;
    public bool isMainScene = false;

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Back))
        {
            OnPressBackButton();
        }
    }

    public void OnPressBackButton()
    {
        if (!isMainScene)
        {
            // 1 for resume needed, 0 for start as normal
            PlayerPrefs.SetInt("isResumeNeeded", 1);
            StartCoroutine(LoadAsyncronously(1));
        }
        else
        {
            StartCoroutine(LoadAsyncronously(0));
        }
    }

    IEnumerator LoadAsyncronously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex, LoadSceneMode.Single);
        LoadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            // unity loads assets in 0 to .9 and activates in .9 to 1
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;
            yield return null;
        }
        LoadingScreen.SetActive(false);
    }
}
