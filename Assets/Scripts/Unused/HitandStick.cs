﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitandStick : MonoBehaviour {

    bool hit = false;
    float depth = 0.30F;
    void OnCollisionEnter(Collision other)
    {
        if (!hit)
        {
            ArrowStick(other);
            hit = true;
        }
    }
    void ArrowStick(Collision col)
    {

        // move the arrow deep inside the enemy or whatever it sticks to
        col.transform.Translate(depth * -Vector2.right);
        // Make the arrow a child of the thing it's stuck to
        transform.parent = col.transform;
        //Destroy the arrow's rigidbody2D and collider2D
       // Destroy(gameObject.rigidbody);
        //Destroy(gameObject.collider);
    }
}
