﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Bow2 : MonoBehaviour
{

    public int theRange = 1000;
    public LineRenderer theLaser = null;
    public GameObject theArrow = null;
    public GameObject parent;
    private Animator anim;
    public float Speed = 1.5f;
    private float touchStartTime = 0f;

    /*
    private bool onGround;
    private float jumpPressure;
    private float minJump;
    private float maxJumpPressure;
    private Rigidbody rbody;*/

    public float timeBetweenShots = 0.3f;  // Allow 3 shots per second
    private float timestamp;

    private Vector3 thePos = Vector3.zero;
    private Vector3 theDirection = Vector3.zero;
    private Vector3 theEnd = Vector3.zero;

    private RaycastHit theHit;
    private bool isOut = false;

    private void Start()
    {
        /*
        onGround = true;
        jumpPressure = 0f;
        minJump = 2f;
        maxJumpPressure = 10f;
        rbody = GetComponent<Rigidbody>();*/
        //edit ends
        
        theLaser = this.gameObject.GetComponent<LineRenderer>();
        anim = parent.GetComponent<Animator>();
       

    }

    private void FixedUpdate()
    {
        thePos = this.transform.position;
        theDirection = this.transform.TransformDirection(Vector3.forward);

        theLaser.SetPosition(0, thePos);

        if (Physics.Raycast(thePos, theDirection, out theHit, theRange))
        {

            theLaser.SetPosition(1, theHit.point);
        
        }

        theEnd = thePos + theDirection * theRange;
        theLaser.SetPosition(1, theEnd);
    

        if (Input.GetKeyDown(KeyCode.Mouse0))
         {

            touchStartTime = Time.time;

         }

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            //float delta = Time.time - TouchStartTime;
           // float adjustedSpeed = speed * delta;
           // GetComponent<Rigidbody>().AddForce(Vector);

        }

        if (isOut)
        {
                //Rigidbody clone;
                GameObject cloneArrow = (GameObject)Instantiate(theArrow, thePos, this.transform.rotation);
                cloneArrow.transform.position = thePos;
                cloneArrow.transform.rotation = this.transform.rotation;
                isOut = false;
        }
    }
}

