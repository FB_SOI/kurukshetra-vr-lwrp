﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disappear : MonoBehaviour {
    public float ArrowLife = 4.0f;

    void Update()
    {
        ArrowLife -= Time.deltaTime;
        if (ArrowLife <= 0)
        {
            Destroy(gameObject);
        }
    }

}
