﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages Spawn Points of perticular character
/// </summary>
[System.Serializable]
public struct SpawnPoints
{
    public CharacterScriptableObject character;
    public Transform[] spawnPoints;
}

/// <summary>
/// player will be instatiated at gameobjects position on which
/// script is attached
/// </summary>
public class PlayerSelection : MonoBehaviour
{
    [Header("Player Properties")]
    public GameObject bowPlayer;
    public Camera bowPlayerCamera;
    public Transform bowHealthUITransform;
    public Transform bowScoreUITransform;
    public GameObject macePlayer;
    public Camera macePlayerCamera;
    public Transform maceHealthUITransform;
    public Transform maceScoreUITransform;

    [HideInInspector]
    public GameObject currentPlayer { get; private set; }

    [HideInInspector]
    public Camera currentPlayerCamera;

    public bool changePlayerToTransform = false;

    [HideInInspector]
    public CharacterStats characterStats;
    public SpawnPoints[] spawnPoints;
    
    private List<SpawnPoints> pandavasSpawnPoints;
    private List<SpawnPoints> kauravasSpawnPoints;
    
    private GameManagement gameManager;

    #region Singleton
    public static PlayerSelection Instance;

    void Awake()
    {
        if (Instance != null)
        {
            throw new System.Exception("Multiple Player Selection instances has been defined");
        }
        Instance = this;

        Initialize();
    }
    #endregion
    
    private void Initialize()
    {
        gameManager = GameManagement.Instance;
        if (gameManager == null)
        {
            throw new MissingReferenceException("Cannont find GameManagement");
        }
        if (changePlayerToTransform)
        {
            InitializeList();
        }
        
        EnablePlayer();
    }

    private void InitializeList()
    {
        pandavasSpawnPoints = new List<SpawnPoints>();
        kauravasSpawnPoints = new List<SpawnPoints>();

        foreach(SpawnPoints point in spawnPoints)
        {
            if (point.character.family == CharacterFamily.Pandava)
            {
                pandavasSpawnPoints.Add(point);
            }
            else
            {
                kauravasSpawnPoints.Add(point);
            }
        }
    }

    private void EnablePlayer()
    {
        //Weapon weapon = (Weapon) PlayerPrefs.GetInt(PlayerPrefsKeys.PLAYER_WEAPON_PREFS, (int) defaultPlayerWeapon);
        Weapon weapon = gameManager.weapon.weapon;
        switch (weapon)
        {
            case Weapon.Bow:
                currentPlayer = bowPlayer;
                currentPlayerCamera = bowPlayerCamera;

                if (bowHealthUITransform)
                {
                    // set health UI target position
                    GetComponent<HealthUI>().target = bowHealthUITransform;
                    ScoreManager.Instance.curvedSettings = bowScoreUITransform.GetComponent<CurvedUI.CurvedUISettings>();
                    ScoreManager.Instance.scoreText = bowScoreUITransform.GetComponentInChildren<TMPro.TextMeshProUGUI>();
                    maceScoreUITransform.gameObject.SetActive(false);
                }
                break;
            case Weapon.Mace:
                currentPlayer = macePlayer;
                currentPlayerCamera = macePlayerCamera;

                if (maceHealthUITransform)
                {
                    // set health UI target position
                    GetComponent<HealthUI>().target = maceHealthUITransform;
                    ScoreManager.Instance.curvedSettings = maceScoreUITransform.GetComponent<CurvedUI.CurvedUISettings>();
                    ScoreManager.Instance.scoreText = maceScoreUITransform.GetComponentInChildren<TMPro.TextMeshProUGUI>();
                    bowScoreUITransform.gameObject.SetActive(false);
                }
                //hintText.text = macePlayerHintText;
                break;
        }
        //ResetCurrentPlayerTransform();
        currentPlayer.SetActive(true);
        characterStats = GetComponent<CharacterStats>();

        Transform tmpTransform;
        if (changePlayerToTransform)
        {
            if (gameManager.player.family == CharacterFamily.Pandava)
            {
                tmpTransform = GetRandomSpawnPoint(pandavasSpawnPoints, weapon);
            }
            else
            {
                tmpTransform = GetRandomSpawnPoint(kauravasSpawnPoints, weapon);
            }
            currentPlayer.transform.position = tmpTransform.position;
            currentPlayer.transform.rotation = tmpTransform.rotation;

            //bowHealthUITransform.parent = transform;
            //maceHealthUITransform.parent = transform;
            
        }
    }
    
    private Transform GetRandomSpawnPoint(List<SpawnPoints> points, Weapon weapon)
    {
        foreach (SpawnPoints point in points)
        {
            if (point.character.weapon.weapon == weapon)
            {
                return point.spawnPoints[Random.Range(0, point.spawnPoints.Length)];
            }
        }

        // if not fount, return current transform
        return transform;
    }
}