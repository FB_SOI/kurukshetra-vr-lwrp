using UnityEngine;

[RequireComponent(typeof(CharacterStats))]
public class CharacterCombat : MonoBehaviour {

	public float attackSpeed = 1f;
	private float attackCooldown = 0f;
    const float combatCooldown = 5;
    float lastAttackTime;

	public float attackDelay = .6f;

    public bool InCombat { get; private set; }
	public event System.Action OnAttack;

    CharacterAnimator characterAnimator;
    [HideInInspector] public CharacterStats myStats { get; private set; }
    CharacterStats opponentStats;

	void Start ()
	{
		myStats = GetComponent<CharacterStats>();
        characterAnimator = GetComponent<CharacterAnimator>();
	}

	void Update ()
	{
		attackCooldown -= Time.deltaTime;

        if (Time.time - lastAttackTime > combatCooldown)
        {
            InCombat = false;
        }
	}

	public void Attack (CharacterStats targetStats)
	{
		if (myStats.currentHealth > 0 && attackCooldown <= 0f && (characterAnimator.animator.GetCurrentAnimatorStateInfo(0).IsName("Combat_idle") || characterAnimator.animator.GetCurrentAnimatorStateInfo(0).IsName("Locomotion")))
		{
            opponentStats = targetStats;
            OnAttack?.Invoke();

            attackCooldown = 1f / attackSpeed;
            InCombat = true;
            lastAttackTime = Time.time;
		}
		
        //if (characterAnimator.animator.GetCurrentAnimatorStateInfo(0).IsName("Combat_idle"))
        //{
        //    Debug.Log("idle anim playing");
        //}
	}

    public void AttackHit_AnimationEvent()
    {
        //Debug.Log("Hitting " + opponentStats.gameObject.name);
        opponentStats.TakeDamage(myStats.damage.GetValue());
        if (opponentStats.currentHealth <= 0)
		{
			InCombat = false;
		}
    }

}
