﻿using UnityEngine;
using TMPro;

/// <summary>
/// Manages Start Scene
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class StartGameManagement : MonoBehaviour
{
    public TextMeshProUGUI headerText;
    public TextMeshProUGUI scoreText;

    [Header("UI Canvases")]
    public GameObject startGameUI;
    public CharacterSelector characterSelectionUI;
    public GameObject weaponSelectionUI;
    public GameObject tutorialUI;
    public GameObject exitGameUI;

    //private bool isCharacterMenuOpen { get { return characterSelectionUI.characterSelectionUI.activeSelf; } }
    //private bool isWeaponMenuOpen { get { return weaponSelectionUI.activeSelf; } }
    private bool isStartMenuOpen { get { return startGameUI.activeSelf; } }
    //private bool isTutorialMenuOpen { get { return tutorialUI.activeSelf; } }
    //private bool isExitMenuOpen { get { return exitGameUI.activeSelf; } }

    private const string headerInitialString = "START";
    private const string headerCharacterSelectionString = "CHARACTER SELECTION";
    private const string headerWeaponSelectionString = "WEAPON SELECTION";
    private const string headerExitString = "EXIT";
    private const string headerTutorialString = "Tutorial";

    private AudioSource audioSource;
    
    public void OnHoverEnter()
    {
        // play the ui hover effect
        audioSource.Play();
    }

    public void OnHoverExit()
    {
        
    }

    public void OnPrimarySelected()
    {
        
    }
    
    public void OnStartGameSelected()
    {
        headerText.text = headerCharacterSelectionString;
        startGameUI.SetActive(false);
        characterSelectionUI.ToogleUI(true);
        weaponSelectionUI.SetActive(false);
        tutorialUI.SetActive(false);
        exitGameUI.SetActive(false);
    }

    public void OnStartTrainingSelected()
    {
        headerText.text = headerWeaponSelectionString;

        DisplayScore();

        startGameUI.SetActive(false);
        characterSelectionUI.ToogleUI(false);
        weaponSelectionUI.SetActive(true);
        tutorialUI.SetActive(false);
        exitGameUI.SetActive(false);
    }

    public void ShowExitUI()
    {
        headerText.text = headerExitString;
        
        startGameUI.SetActive(false);
        characterSelectionUI.ToogleUI(false);
        weaponSelectionUI.SetActive(false);
        tutorialUI.SetActive(false);
        exitGameUI.SetActive(true);
    }

    public void ShowTutorialUI()
    {
        headerText.text = headerTutorialString;

        startGameUI.SetActive(false);
        characterSelectionUI.ToogleUI(false);
        weaponSelectionUI.SetActive(false);
        tutorialUI.SetActive(true);
        exitGameUI.SetActive(false);
    }

    public void OnExitGameSelected()
    {
        Application.Quit();
    }

    public void OnCancleSelected()
    {
        OpenInitialUI();
    }

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        OpenInitialUI();    
    }

    private void DisplayScore()
    {
        int score = PlayerPrefs.GetInt(PlayerPrefsKeys.PLAYER_HIGH_SCORE, 0);
        scoreText.text = string.Format(Strings.SCORE_TEXT, score);
    }

    private void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Back))
        {
            OnBackPressed();
        }
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Q))
        {
            OnBackPressed();
        }
#endif
    }
    
    private void OnBackPressed()
    {
        Debug.Log("Back Pressed");
        //if (isCharacterMenuOpen || isWeaponMenuOpen)
        //{
        //    OpenInitialUI();
        //}
        if (isStartMenuOpen)
        {
            ShowExitUI();
        }
        else
        {
            OpenInitialUI();
        }
    }

    void OpenInitialUI()
    {
        headerText.text = headerInitialString;

        DisplayScore();

        startGameUI.SetActive(true);
        characterSelectionUI.ToogleUI(false);
        weaponSelectionUI.SetActive(false);
        exitGameUI.SetActive(false);
        tutorialUI.SetActive(false);
    }

}
