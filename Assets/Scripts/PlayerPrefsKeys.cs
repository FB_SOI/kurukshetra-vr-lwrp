﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsKeys : MonoBehaviour
{
    public const string PLAYER_WEAPON_PREFS = "PLAYER_WEAPON";
    public const string OPPONENT_WEAPON_PREFS = "OPPONENT_WEAPON";
    public const string PLAYER_HIGH_SCORE = "PLAYER_HIGH_SCORE";
}
