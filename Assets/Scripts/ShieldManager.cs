﻿using UnityEngine;

/// <summary>
/// Manages shield settings
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class ShieldManager : MonoBehaviour
{
    // shield
    public GameObject shield;
    public AudioClip shieldActivateSFX;
    public AudioClip shieldDeactivateSFX;

    private AudioSource audioSource;

    // Start is called before the first frame update
    void Awake()
    {
        shield.SetActive(false);
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // making shield work or not
        if (OVRInput.GetDown(OVRInput.Touch.One))
        {
            ActivateShield();
        }
        if (OVRInput.GetUp(OVRInput.Touch.One))
        {
            DeactivateShield();
        }
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            ActivateShield();
        }
        if (Input.GetMouseButtonUp(0))
        {
            DeactivateShield();
        }
#endif
    }

    void ActivateShield()
    {
        shield.SetActive(true);
        audioSource.PlayOneShot(shieldActivateSFX);
    }

    void DeactivateShield()
    {
        shield.SetActive(false);
        audioSource.PlayOneShot(shieldDeactivateSFX);
    }
}
