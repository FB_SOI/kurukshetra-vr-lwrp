﻿using UnityEngine.UI;
using TMPro;
using UnityEngine.Video;
using UnityEngine;
using System.Collections;

/// <summary>
/// Displayes tutorial info.
/// </summary>
[RequireComponent(typeof(VideoPlayer))]
public class TutorialManager : MonoBehaviour
{
    public TutorialScriptableObject[] tutorials;

    public TextMeshProUGUI headerText;
    public TextMeshProUGUI tutorialText;

    public Image staticImage;

    public RawImage videoRawImage;

    private VideoPlayer videoPlayer;
    private int currentTutorial;

    public void DisplayNextTutorial()
    {
        if (tutorials.Length > 0)
        {
            currentTutorial = (currentTutorial + 1) % tutorials.Length;

            DisplayTutorial(tutorials[currentTutorial]);
        }
    }

    public void DisplayPreviousTutorial()
    {
        if (tutorials.Length > 0)
        {
            if (currentTutorial == 0)
            {
                currentTutorial = tutorials.Length - 1;
            }
            else
            {
                currentTutorial--;
            }
            
            DisplayTutorial(tutorials[currentTutorial]);
        }
    }

    private void DisplayTutorial(TutorialScriptableObject tutorial)
    {
        //Debug.Log("Displaying " + tutorial.name);
        tutorialText.text = tutorial.tutorialText;
        headerText.text = tutorial.name;

        staticImage.sprite = tutorial.sprite;

        if (tutorial.videoClip)
        {
            StartCoroutine(PlayVideo(tutorial.videoClip));
        }
        else
        {
            videoRawImage.texture = tutorial.videoClipDefaultTexture;
        }
    }

    IEnumerator PlayVideo(VideoClip clip)
    {
        // set video player properties
        videoPlayer.clip = clip;
        videoPlayer.audioOutputMode = VideoAudioOutputMode.None;

        // wait for video player to prepare
        videoPlayer.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(1);
        while (!videoPlayer.isPrepared)
        {
            yield return waitForSeconds;
            break;
        }

        //Debug.Log("playing video");
        videoRawImage.texture = videoPlayer.texture;
        videoPlayer.Play();
    }

    // display tutorial when enabled
    private void OnEnable()
    {
        currentTutorial = -1;
        DisplayNextTutorial();
    }

    private void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer>();

        videoPlayer.Prepare();
    }
}
