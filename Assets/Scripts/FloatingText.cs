﻿using UnityEngine;

public class FloatingText : MonoBehaviour
{
    public float destroyTime = 3f;
    public Vector3 offset = new Vector3(0, 2, 0);
    public Vector3 randomizeIntesity = new Vector3(0.5f, 0, 0);

    private Camera cameraToLookAt;

    // Start is called before the first frame update
    void Start()
    {
        cameraToLookAt = PlayerSelection.Instance.currentPlayerCamera;

        Destroy(gameObject, destroyTime);

        transform.localPosition += offset;
        transform.localPosition += new Vector3(
                Random.Range(-randomizeIntesity.x, randomizeIntesity.x),
                Random.Range(-randomizeIntesity.y, randomizeIntesity.y),
                Random.Range(-randomizeIntesity.z, randomizeIntesity.z)
            );
    }

    private void LateUpdate()
    {
        transform.LookAt(cameraToLookAt.transform);
        transform.rotation = Quaternion.LookRotation(cameraToLookAt.transform.forward);
    }
}
