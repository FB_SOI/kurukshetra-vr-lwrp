﻿using UnityEngine;

/// <summary>
/// Manages  Arrow Settings
/// </summary>
public class Arrow : MonoBehaviour
{
    public delegate void OnHitEvent(CharacterStats targetStats);
    public OnHitEvent onHitEvent;

    public float impactRadiius = 5f;
    public LayerMask impactLayerMask;
    //public GameObject[] impactFx;

    private bool isAttached = false;
    private bool isFired = false;
    private ArrowManager arrowManager;
    private bool isShowingExitGUI;

    #region Delegates
    private void OnEnable()
    {
        // listen on changes of game exit ui
        GameManagement.Instance.OnExitUIStatusChanged += OnShowingExitUI;
    }

    private void OnDisable()
    {
        // remove listners
        GameManagement.Instance.OnExitUIStatusChanged -= OnShowingExitUI;
    }
    #endregion

    void OnTriggerStay()
    {
        AttachArrow();
    }

    void OnShowingExitUI(bool status)
    {
        isShowingExitGUI = status;
    }

    void OnTriggerEnter(Collider other)
    {
        // ArrowManager.Instance.DebugText(other.tag);
        if (!isShowingExitGUI)
        {
            if (other.CompareTag(Strings.BOW_TAG))
            {
                AttachArrow();
                return;
            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        //Debug.Log(other.collider.name);
        ContactPoint contact = other.contacts[0];

        Collider[] hitColliders = Physics.OverlapSphere(contact.point, impactRadiius, impactLayerMask);

        foreach(var col in hitColliders)
        {
            CharacterStats stats = col.transform.GetComponent<CharacterStats>();
            if (stats)
            {
                onHitEvent?.Invoke(stats);
            }
        }

        //if (other.transform.CompareTag("Destructables"))
        //{
        //    //IHitable hitable = other.transform.GetComponent(typeof(IHitable)) as IHitable;
        //    //hitable.Hit();
        //    CharacterStats stats = other.transform.GetComponent<CharacterStats>();
        //    if (stats)
        //    {
        //        onHitEvent.Invoke(stats);
        //    }
        //}

        Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
        Vector3 pos = contact.point;

        //var hitPrefab = impactFx[Random.Range(0, impactFx.Length)];
        GameObject hitVFX;
        if (!arrowManager.objectPooler.TryGetNextObject(pos, rot, out hitVFX))
        {
            Debug.LogError(string.Format(Strings.UNABLE_TO_SPAWN, "Impact FX"));
        }

        //if (hitPrefab != null)
        //{
        //    var hitVFX = Instantiate(hitPrefab, pos, rot);
        //    var ps = hitVFX.GetComponent<ParticleSystem>();
        //    if (ps == null)
        //    {
        //        var psChild = hitVFX.transform.GetChild(0).GetComponent<ParticleSystem>();
        //        Destroy(hitVFX, psChild.main.duration);
        //    }
        //    else
        //        Destroy(hitVFX, ps.main.duration);
        //}

        Destroy(gameObject);
    }

    //private void DestroyEnemy(GameObject other, float time)
    //{
    //    Destroy(other, time);
    //}

    void Update()
    {
        if (isFired && transform.GetComponent<Rigidbody>().velocity.magnitude > 5f)
        {
            transform.LookAt(transform.position + transform.GetComponent<Rigidbody>().velocity);
        }
    }

    private void Start()
    {
        arrowManager = ArrowManager.Instance;
    }

    public void Fired()
    {
        isFired = true;
        Destroy(this.gameObject, 5f);
    }

    private void AttachArrow()
    {
        //if (!isAttached && (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || Input.GetMouseButtonDown(0)))
        if (!isAttached && OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {
            arrowManager.AttachBowToArrow();
            isAttached = true;
        }

#if UNITY_EDITOR
        if (!isAttached && Input.GetMouseButton(0))
        {
            arrowManager.AttachBowToArrow();
            isAttached = true;
        }
#endif

    }

}