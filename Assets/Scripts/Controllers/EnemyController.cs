using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Controls the Enemy AI
/// </summary>
[RequireComponent(typeof(EnemyStats))]
public class EnemyController : MonoBehaviour {

    //public float lookRadius = 10f;	// Detection range for player
    [HideInInspector] public float stoppingDistance = 5f;

    private Transform target;   // Reference to the player
    private NavMeshAgent agent; // Reference to the NavMeshAgent
    private CharacterCombat combat;
    private EnemyStats stats;
    private Vector3 currentPlayerTransformPosition;

    public void Initialize(float speed = 2f, int maxHealth = 100)
    {
        agent = GetComponent<NavMeshAgent>();
        stats = GetComponent<EnemyStats>();
        agent.speed = speed;
        stats.maxHealth = maxHealth;
    }

    public void SetTargetPosition(Vector3 targetPosition)
    {
        agent.SetDestination(targetPosition);
    }

    // Use this for initialization
    void Start () {
		target = PlayerManager.instance.player.transform;
		combat = GetComponent<CharacterCombat>();
        
        currentPlayerTransformPosition = PlayerSelection.Instance.currentPlayer.transform.position;

        // set stopping distance
        if (GameManagement.Instance.player.weapon.weapon == Weapon.Mace)
        {
            stoppingDistance = 5f;
        }
        else
        {
            stoppingDistance = 13f;
        }
        
        // Move towards the target
        SetTargetPosition(currentPlayerTransformPosition);
    }
	
	// Update is called once per frame
	void Update () {
        // Distance to the target
        float distance = Vector3.Distance(currentPlayerTransformPosition, transform.position);

        //// Move towards the target
        //agent.SetDestination(PlayerSelection.instance.currentPlayer.transform.position);

        // If within attacking distance
        if (distance <= stoppingDistance)
        {
            CharacterStats targetStats = target.GetComponent<CharacterStats>();
            if (targetStats != null)
            {
                //Debug.Log("Attack...!");
                combat.Attack(targetStats);
            }

            FaceTarget();   // Make sure to face towards the target
        }
    }

	// Rotate to face the target
	void FaceTarget ()
	{
		Vector3 direction = (currentPlayerTransformPosition - transform.position).normalized;
		Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
		transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
	}

    // Show the lookRadius in editor
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, stoppingDistance);
    }
}
