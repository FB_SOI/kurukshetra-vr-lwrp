﻿using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;

/// <summary>
/// Scenes with build indexes
/// </summary>
public enum Scenes
{
    _preload = 0,
    Start = 1,
    Arena = 2,
    Game = 3,
    _hints = 4,
}

/// <summary>
/// Handle game logic persisting between scenes
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class GameManagement : MonoBehaviour
{
    #region Singleton

    public static GameManagement Instance;

    void Awake()
    {
        if (Instance != null)
        {
            Debug.LogWarning("More than one instance of GameManagement found!");
            return;
        }

        Instance = this;
    }

    #endregion

    [Header("Player Info")]
    public CharacterScriptableObject player;
    public CharacterScriptableObject opponent;
    public WeaponScriptableObject weapon;

    [Header("Game UI")]
    public GameObject exitGameUI;
    public TMPro.TextMeshProUGUI highScoreText;
    public GameObject exitPrompt;
    public GameObject exitNoPrompt;

    public ControllerSelection.OVRPointerVisualizer pointerVisualizer;

    private AudioSource audioSource;
    private float lowRayDrawDistanace = 0f;
    private float midRayDrawDistanace = 1.5f;
    private float highRayDrawDistanace = 5f;

    //[HideInInspector]
    //public bool isShowingExitGUI { get; private set; }

    /// <summary>
    /// Actions to be invoked when exit ui prompt is shown
    /// Params: Boolean for current status
    /// </summary>
    public System.Action<bool> OnExitUIStatusChanged;

    private Canvas exitGameCanvas;
    private AudioManager audioManager;
    private Scenes currentScene = Scenes._preload;
    private float hintScreenShowTime = 8f;
    private float exitGameUIDistance = 5f;
    
    public void SavePlayer(CharacterScriptableObject character)
    {
        player = character;
        weapon = character.weapon;
    }

    public void SaveOpponent(CharacterScriptableObject character)
    {
        opponent = character;
    }

    public void SavePlayer(WeaponScriptableObject weapon)
    {
        this.weapon = weapon;
    }

    public void LoadScene(Scenes scene)
    {
        StartCoroutine(LoadWithHintScene(scene));
    }

    /// <summary>
    /// shows exit UI
    /// </summary>
    /// <param name="prompt">Whether or not show yes/no prompt</param>
    public void ShowExitGUI(bool prompt)
    {
        pointerVisualizer.rayDrawDistance = highRayDrawDistanace;

        var playerSelection = PlayerSelection.Instance;

        // set event camera
        var cam = playerSelection.currentPlayerCamera;
        if (cam)
        {
            exitGameCanvas.worldCamera = playerSelection.currentPlayerCamera;
        }
        
        //pause game
        Time.timeScale = 0f;
        
        // set UI location
        exitGameUI.transform.position = playerSelection.currentPlayer.transform.position 
            + playerSelection.currentPlayer.transform.forward * exitGameUIDistance;

        exitGameUI.transform.rotation = new Quaternion(0.0f, playerSelection.currentPlayer.transform.rotation.y,
            0.0f, playerSelection.currentPlayer.transform.rotation.w);

        // set current score
        highScoreText.text = ScoreManager.Instance.score.ToString();
        
        exitGameUI.SetActive(true);

        if (prompt)
        {
            exitPrompt.SetActive(true);
            exitNoPrompt.SetActive(false);
        }
        else
        {
            exitPrompt.SetActive(false);
            exitNoPrompt.SetActive(true);
        }

        // call listners if any
        OnExitUIStatusChanged?.Invoke(true);
    }

    public void HideExitGUI()
    {
        pointerVisualizer.rayDrawDistance = lowRayDrawDistanace;

        //end pause 
        Time.timeScale = 1f;

        exitGameUI.SetActive(false);

        // call listners if any
        OnExitUIStatusChanged?.Invoke(false);
    }

    public void OnExit()
    {
        HideExitGUI();
        ScoreManager.Instance.SaveScore();
        LoadPreviousScene();
    }

    public void OnHoverEnter()
    {
        // play hover sound fx
        audioSource.Play();
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (currentScene == Scenes._preload)
        {
            return;
        }

        // disable selectionVisualizer on game and arena scene
        if (currentScene == Scenes.Arena || currentScene == Scenes.Game)
        {
            pointerVisualizer.rayDrawDistance = lowRayDrawDistanace;
        }
        else if (currentScene == Scenes.Start)
        {
            pointerVisualizer.rayDrawDistance = highRayDrawDistanace;
        }
        else
        {
            pointerVisualizer.rayDrawDistance = midRayDrawDistanace;
        }

        //Debug.Log(scene.name + " Loaded... !");
        audioManager.Play(scene.name);
    }

    private void LoadPreviousScene()
    {
        if (currentScene == Scenes.Arena || currentScene == Scenes.Game)
        {
            LoadScene(Scenes.Start);
        }
        else
        {
            Application.Quit();
        }
    }
    
    //private void PlaySoundFX()
    //{
    //    audioManager.Play(currentScene.ToString());
    //}

    private IEnumerator LoadWithHintScene(Scenes scene)
    {
        currentScene = Scenes._hints;
        SceneManager.LoadScene((int)currentScene, LoadSceneMode.Single);

        //PlaySoundFX();

        yield return new WaitForSeconds(hintScreenShowTime);

        currentScene = scene;
        SceneManager.LoadScene((int)scene, LoadSceneMode.Single);
        //PlaySoundFX();
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        exitGameCanvas = GetComponentInChildren<Canvas>(true);
        exitGameUI.SetActive(false);

        audioManager = AudioManager.Instance;
        StartCoroutine(LoadWithHintScene(Scenes.Start));
    }

    //IEnumerator LoadStartScene()
    //{
    //    yield return new WaitForSeconds(3f);
    //    LoadScene(Scenes.Start);
    //}
    
    private void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Back))
        {
            OnBackPressed();
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Q))
        {
            OnBackPressed();
        }
#endif
    }

    private void OnBackPressed()
    {
        if (currentScene == Scenes.Game)
        {
            ShowExitGUI(true);
        }
        else if (currentScene == Scenes.Arena)
        {
            LoadPreviousScene();
        }
    }
}
