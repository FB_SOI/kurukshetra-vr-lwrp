﻿using UnityEngine;
using UnityEngine.Video;

/// <summary>
/// Contains game tutorial related information
/// </summary>
[CreateAssetMenu(fileName = "Tutorial", menuName = "ScriptableObjects/Tutorial", order = 1)]
public class TutorialScriptableObject : ScriptableObject
{
    public new string name;

    [TextArea(3, 5)]
    public string tutorialText;

    [Tooltip("Sprite is Mandatory.")]
    public Sprite sprite;

    [Tooltip("Videoclip is optional.")]
    public VideoClip videoClip;

    [Tooltip("Provide this if videoclip is not available.")]
    public Texture videoClipDefaultTexture;
}
