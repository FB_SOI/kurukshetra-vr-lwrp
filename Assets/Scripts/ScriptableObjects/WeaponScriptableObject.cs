﻿using UnityEngine;

public enum Weapon { Bow = 0, Mace = 1, }

[CreateAssetMenu(fileName = "Weapon", menuName = "ScriptableObjects/Weapon", order = 1)]
public class WeaponScriptableObject : ScriptableObject
{
    public Sprite weaponSprite;

    public Weapon weapon;

    [TextArea(3, 5)]
    public string weaponDescription;

    [TextArea(5, 7)]
    public string usage;
}
