﻿using UnityEngine;

public enum CharacterFamily { Pandava = 0, Kaurava = 1, }

[CreateAssetMenu(fileName = "Character", menuName = "ScriptableObjects/Character", order = 1)]
public class CharacterScriptableObject : ScriptableObject
{
    public Sprite characterSprite;

    public string characterName;

    [TextArea(3, 5)]
    public string characterDescription;

    public WeaponScriptableObject weapon;

    public CharacterFamily family;
}
