﻿using UnityEngine;

/// <summary>
/// Trivia 
/// </summary>
[CreateAssetMenu(fileName = "Trivia", menuName = "ScriptableObjects/Trivia", order = 1)]
public class TriviaScriptableObject : ScriptableObject
{
    public string header;

    [TextArea(3, 5)]
    public string trivia;
}
