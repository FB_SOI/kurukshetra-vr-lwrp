﻿using UnityEngine;

/// <summary>
/// Hitable Destructible Target
/// Spawns destroyed version of target on hit
/// </summary>
public class DestructibleTarget : MonoBehaviour, IHitable
{
    //public GameObject destroyedVersion;
    
    [SerializeField]
    float oppositeForce = 10f;
    
    //[SerializeField]
    //ParticleSystem shootEffect;
    //[SerializeField]
    //GameObject fireBallEffect;

    private CannonPoolManager poolManager;

    public void Hit()
    {
        // get opposite velocity of object
        var oppose = - GetComponent<Rigidbody>().velocity;

        //shootEffect.Play(true);

        //var obj = Instantiate(destroyedVersion, transform.position, transform.rotation) as GameObject;

        GameObject obj;
        if (CannonPoolManager.Instance.destroyedCannonPool.TryGetNextObject(transform.position, transform.rotation, out obj))
        {
            obj.GetComponent<AudioSource>().Play();
            
            // apply force to each child
            foreach (Transform child in obj.transform)
            {
                // reset child transform
                child.transform.localPosition = Vector3.zero;

                Rigidbody rb = child.GetComponent<Rigidbody>();

                if (rb)
                {
                    // zero-out velocities to nullify previous speeds <due to pooling>
                    rb.angularVelocity = Vector3.zero;
                    rb.velocity = Vector3.zero;

                    rb.AddForce((oppositeForce * oppose.normalized) * Time.deltaTime);
                }
            }
        }
        else // can't spawn
        {
            Debug.LogError(string.Format(Strings.UNABLE_TO_SPAWN, "Destroyed Cannons"));
        }
    }
}
