﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PrefsEditor : MonoBehaviour
{
    public string KEY = PlayerPrefsKeys.PLAYER_WEAPON_PREFS;
    public int VALUE;
    public bool edit = false;

    private bool oldEdit = false;

    // Start is called before the first frame update
    void Start()
    {
        VALUE = PlayerPrefs.GetInt(KEY);
    }

    // Update is called once per frame
    void Update()
    {
        if (edit && oldEdit != edit)
        {
            PlayerPrefs.SetInt(KEY, VALUE);
            Debug.Log("SAVED: " + KEY + " : " + VALUE);
            oldEdit = edit;
        }
        if (!edit)
        {
            VALUE = PlayerPrefs.GetInt(KEY);
        }
    }
}
