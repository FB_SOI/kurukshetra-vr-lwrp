﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SpellProjector : MonoBehaviour
{
    public Transform spawnPoint;
    public float speed = 10f;
    public float fireRate = 0.7f;
    
    private WaitForSeconds shootDuration = new WaitForSeconds(1f);
    private float nextFireTime;
    private CharacterStats myStats;
    private MaceManager maceManager;
    private bool isShowingExitGUI = false;

    private AudioSource audioSource;
    private GameManagement gameManager;

    #region Delegates
    private void OnEnable()
    {
        gameManager = GameManagement.Instance;
        // listen on changes of game exit ui
        gameManager.OnExitUIStatusChanged += OnShowingExitUI;
    }

    private void OnDisable()
    {
        // remove listners
        gameManager.OnExitUIStatusChanged -= OnShowingExitUI;
    }
    #endregion

    void OnShowingExitUI(bool status)
    {
        isShowingExitGUI = status;
    }
    
    private void Start()
    {
        myStats = PlayerSelection.Instance.characterStats;
        maceManager = MaceManager.Instance;

        audioSource = GetComponent<AudioSource>();

        //myStats = PlayerSelection.instance.currentPlayer.GetComponent<PlayerStats>();
        if (!myStats)
        {
            throw new MissingReferenceException("Missing CharaterStats");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isShowingExitGUI)
        {
            // go into slow motion only if button is pressed and when you can fire
            if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger) && Time.time > nextFireTime)
            {
                Time.timeScale = 0.5f;
            }
            if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) && Time.time > nextFireTime)
            {
                Time.timeScale = 1f;
                ProjectSpell();
            }
        }
        
#if UNITY_EDITOR
        if (Input.GetMouseButtonUp(0) && Time.time > nextFireTime)
        {
            ProjectSpell();
        }
#endif
    }

    private void ProjectSpell()
    {
        nextFireTime = Time.time + fireRate;

        // Get Spell from pool
        GameObject spell;
        if (maceManager.objectPooler.TryGetNextObject(spawnPoint.position, spawnPoint.rotation, out spell))
        {
            var obj = spell.GetComponent<Spell>();

            // register for hit events
            if (obj)
            {
                obj.onHitEvent += OnHitEvent;
            }

            //Rigidbody spellRB = spell.GetComponent<Rigidbody>();
            //spellRB.useGravity = false;
            //spellRB.velocity = spell.transform.forward * speed;
            StartCoroutine(ShootEffect());
            //Destroy(spell, 3f);
        }
        else // can't get object from pool
        {
            Debug.LogError(string.Format(Strings.UNABLE_TO_SPAWN, "Projectile"));
        }
    }

    public void OnHitEvent(CharacterStats targetStats)
    {
        targetStats.TakeDamage(myStats.damage.GetValue());
    }

    private IEnumerator ShootEffect()
    {
        audioSource.Play();
        yield return shootDuration;
    }
    
}
