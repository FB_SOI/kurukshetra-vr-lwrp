﻿using UnityEngine;

public class Mace : MonoBehaviour
{
    private SpellProjector spellProjector;

    private void Awake()
    {
        spellProjector = GetComponent<SpellProjector>();
        if (!spellProjector)
        {
            throw new MissingComponentException("SpellProjector Missing");
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        //Debug.Log("Mace: " + other.collider.name);
        if (other.transform.CompareTag("Destructables"))
        {
            CharacterStats stats = other.transform.GetComponent<CharacterStats>();
            if (stats)
            {
                spellProjector.OnHitEvent(stats);
            }
        }
    }

}
