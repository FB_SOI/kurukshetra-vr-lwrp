﻿using EZObjectPools;
using UnityEngine;

/// <summary>
/// Pools Cannon gameobjects and destroyed versions
/// </summary>
public class CannonPoolManager : MonoBehaviour
{
    #region Singleton
    public static CannonPoolManager Instance;
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            throw new System.Exception(string.Format(Strings.SINGLETON_EXISTS, "CannonPoolManager"));
    }
    #endregion
    public EZObjectPool cannonObjectPool;
    public EZObjectPool destroyedCannonPool;
}
