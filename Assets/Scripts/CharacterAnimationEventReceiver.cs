﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationEventReceiver : MonoBehaviour {

    public CharacterCombat combat;

    public void AttackHitEvent()
    {
        //Debug.Log("Hit Event Occured by " + transform.name);
        combat.AttackHit_AnimationEvent();
    }
    
}
