﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterStats))]
public class HealthUI : MonoBehaviour {
    public GameObject uiPrefab;
    public Transform target;

    //public GameObject floatingTextPrefab;

    [Header("For Hit Spawn Effect")]
    public bool isShowHitEffect = true;
    public Vector3 offset = new Vector3(0, 2, 0.5f);
    public Vector3 randomizeIntesity = new Vector3(0.5f, 0, 0);

    private Transform hitSpawnTransform;
    private EnemySpawnManager enemySpawnManager;

    float visibleTime = 5;
    float lastMadeVisibleTime;
    Transform ui;
    Image healthSlider;
    Transform playerTransform;
    
	// Use this for initialization
	void Start () {
        playerTransform = PlayerSelection.Instance.currentPlayer.transform;
        enemySpawnManager = EnemySpawnManager.Instance;

        //cam = Camera.main.transform;
        //cam = PlayerSelection.instance.GetComponentInChildren<Camera>().transform;
        //cam = transform;

        var c = PlayerManager.instance.uiCanvas;
        if (c.renderMode == RenderMode.WorldSpace)
        {
            //// set event camera
            //c.worldCamera = Camera.main;

            ui = Instantiate(uiPrefab, c.transform.position, c.transform.rotation, c.transform).transform;
            healthSlider = ui.GetChild(0).GetComponent<Image>();
            ui.gameObject.SetActive(false);
        }

        GetComponent<CharacterStats>().OnHealthChanged += OnHealthChanged;

        // set hit spawntransform
        hitSpawnTransform = transform;
        hitSpawnTransform.localPosition += offset;
    }

    void OnHealthChanged(int maxHealth, int currentHealth) {
        if (ui != null)
        {
            ui.gameObject.SetActive(true);
            lastMadeVisibleTime = Time.time;

            float healthPercent = (float)currentHealth / maxHealth;
            healthSlider.fillAmount = healthPercent;
            if (currentHealth <= 0)
            {
                Destroy(ui.gameObject);
            }
            else if (isShowHitEffect)
            {
                ShowFloatingText(currentHealth);
            }
        }
    }

    private void ShowFloatingText(int currentHealth)
    {
        //var go = Instantiate(floatingTextPrefab, transform.position, Quaternion.identity, transform);
        //go.GetComponent<TextMesh>().text = currentHealth.ToString();

        hitSpawnTransform.localPosition += new Vector3(
                Random.Range(-randomizeIntesity.x, randomizeIntesity.x),
                Random.Range(-randomizeIntesity.y, randomizeIntesity.y),
                Random.Range(-randomizeIntesity.z, randomizeIntesity.z)
            );
        //hitSpawnTransform.position = hitSpawnTransform.position + Vector3.forward * 0.3f;
        enemySpawnManager.enemyImapactPool.TryGetNextObject(hitSpawnTransform.position, Quaternion.Euler(-90, 0, 0));
        PlayImpactFX();
        //Instantiate(floatingTextPrefab, hitSpawnTransform.position, Quaternion.identity);
    }

    private void PlayImpactFX()
    {
        GameObject go; 
        if (enemySpawnManager.impactSoundObjectPool.TryGetNextObject(hitSpawnTransform.position, Quaternion.identity, out go))
        {
            go.GetComponent<AudioSource>().Play();
        }
    }

    void LateUpdate () {
        if (ui != null)
        {
            ui.position = target.position;
            ui.forward = -playerTransform.forward;

            if (Time.time - lastMadeVisibleTime > visibleTime)
            {
                ui.gameObject.SetActive(false);
            }
        }
	}
}
