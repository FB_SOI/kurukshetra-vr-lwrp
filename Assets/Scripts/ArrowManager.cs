﻿using UnityEngine;
using UnityEngine.UI;
using EZObjectPools;
using System;

[RequireComponent(typeof(AudioSource))]
public class ArrowManager : MonoBehaviour
{
    public OVRTrackedRemote trackedObj;
    public GameObject stringAttachPoint;
    public GameObject arrowStartPoint;
    public GameObject stringStartPoint;

    public GameObject arrowPrefab;
    public EZObjectPool objectPooler;

    public AudioClip bowStringStrechedClip;
    public AudioClip arrowShootClip;

    private GameObject currentArrow;
    private CharacterStats myStats;
    private bool isAttached = false;
    private AudioSource audioSource;
    private bool isShowingExitGUI;

    private GameManagement gameManager;

    #region Singleton
    public static ArrowManager Instance;
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            throw new Exception(string.Format(Strings.SINGLETON_EXISTS, "MaceManager"));

        gameManager = GameManagement.Instance;
    }
    void OnDestroy()
    {
        if (Instance == this)
            Instance = null;
    }
    #endregion

    #region Delegates
    private void OnEnable()
    {
        // listen on changes of game exit ui
        gameManager.OnExitUIStatusChanged += OnShowingExitUI;
    }

    private void OnDisable()
    {
        // remove listners
        gameManager.OnExitUIStatusChanged -= OnShowingExitUI;
    }
    #endregion

    public void AttachBowToArrow()
    {
        currentArrow.transform.parent = stringAttachPoint.transform;
        currentArrow.transform.position = arrowStartPoint.transform.position;
        currentArrow.transform.rotation = arrowStartPoint.transform.rotation;

        isAttached = true;

        PlayStringStrechedFX();

        // when attached to bow, go into slow motion
        Time.timeScale = 0.5f;
    }

    public void OnHitEvent(CharacterStats targetStats)
    {
        targetStats.TakeDamage(myStats.damage.GetValue());
    }

    private void Start()
    {
        myStats = PlayerSelection.Instance.characterStats;
        audioSource = GetComponent<AudioSource>();

        //myStats = PlayerSelection.instance.currentPlayer.GetComponent<PlayerStats>();
        if (!myStats)
        {
            throw new MissingReferenceException("Missing CharaterStats");
        }
    }
    
    void OnShowingExitUI(bool status)
    {
        isShowingExitGUI = status;
    }

    void Update()
    {
        AttachArrow();
        PullString();
    }

    private void PullString()
    {
        if (isAttached)
        {
            float dist = (stringStartPoint.transform.position - trackedObj.transform.position).magnitude;
            stringAttachPoint.transform.localPosition = stringStartPoint.transform.localPosition + new Vector3(5f * dist, 0f, 0f);
            //DrawPath();

            //var device = SteamVR_Controller.Input((int)trackedObj);
            //if (device.GetTouchUp (SteamVR_Controller.ButtonMask.Trigger)) {
            //if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) || Input.GetMouseButtonUp(0))
            if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger))
            {
                Fire();
            }
#if UNITY_EDITOR
            if (Input.GetMouseButtonUp(0))
            {
                Fire();
            }
#endif
        }
    }
    
    private void Fire()
    {
        if (!isShowingExitGUI)
        {
            float dist = (stringStartPoint.transform.position - trackedObj.transform.position).magnitude;

            currentArrow.transform.parent = null;

            var arrow = currentArrow.GetComponent<Arrow>();
            arrow.onHitEvent += OnHitEvent;
            arrow.Fired();

            Rigidbody r = currentArrow.GetComponent<Rigidbody>();
            r.velocity = currentArrow.transform.forward * 50f * dist;
            r.useGravity = true;

            currentArrow.GetComponent<Collider>().isTrigger = false;

            stringAttachPoint.transform.position = stringStartPoint.transform.position;

            currentArrow = null;
            isAttached = false;

            PlayArrowShootFX();

            // when fired, end slow motion
            Time.timeScale = 1f;
        }
    }


    private void AttachArrow()
    {
        if (currentArrow == null)
        {
            currentArrow = Instantiate(arrowPrefab);
            currentArrow.transform.parent = trackedObj.transform;
            currentArrow.transform.localPosition = new Vector3(0f, 0f, .342f);
            currentArrow.transform.localRotation = Quaternion.identity;
        }
    }

    private void PlayStringStrechedFX()
    {
        audioSource.Stop();
        audioSource.PlayOneShot(bowStringStrechedClip);
    }

    private void PlayArrowShootFX()
    {
        audioSource.Stop();
        audioSource.PlayOneShot(arrowShootClip);
    }
}
