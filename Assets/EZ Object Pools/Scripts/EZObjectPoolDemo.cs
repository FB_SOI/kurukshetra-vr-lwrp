﻿using EZObjectPools;
using UnityEngine;

public class EZObjectPoolDemo : MonoBehaviour
{
    public EZObjectPool objectPools;
    public Transform spawnTransform;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            SpawnNext();
        }
    }

    void SpawnNext()
    {
        objectPools.TryGetNextObject(spawnTransform.position, spawnTransform.rotation);
    }
}
